import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../shared/services/auth.service';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss'],
})
export class UserLoginComponent {

  constructor(
    public auth: AuthService,
    private router: Router
  ) {
  }

  // private afterSignIn() {
  //   this.router.navigate(['/dashboard']);
  // }

  logOut() {
    this.auth.signOut().then(
      () => { console.log( 'Logged out' ); },
      (err) => { console.log( err ); }
    );
  }

}
