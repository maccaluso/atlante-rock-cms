import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { WidgetsModule } from '../shared/widgets/widgets.module';

import { UserLoginComponent } from './user-login.component';

import { AuthService } from '../shared/services/auth.service';
import { NotifyService } from '../shared/services/notify.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    FontAwesomeModule,
    WidgetsModule
  ],
  declarations: [
    UserLoginComponent
  ],
  providers: [
    AuthService,
    NotifyService
  ]
})
export class UserLoginModule { }
