import { Component, EventEmitter, OnInit, Input, Output, NgZone } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Http } from '@angular/http';
import { FileUploader, FileUploaderOptions, ParsedResponseHeaders } from 'ng2-file-upload';
import { Cloudinary } from '@cloudinary/angular-5.x';

@Component({
	selector: 'form-uploader-button',
	templateUrl: 'form-uploader-button.component.html'
})
export class FormUploaderButtonComponent implements OnInit {

	@Input() responses: Array<any>;
	@Input() icon: any;
	@Input() label: string = 'Upload files';

	@Output() onUploadResult = new EventEmitter<any>();
	@Output() onAfterAddingFile = new EventEmitter<any>();

	hasBaseDropZoneOver: boolean = false;
	uploader: FileUploader;
	private title: string;
	public filePreviewPath: SafeUrl;

	constructor(
		private cloudinary: Cloudinary,
		private zone: NgZone,
		private http: Http,
		private sanitizer: DomSanitizer
		) {
		this.responses = [];
		this.title = '';
	}

	ngOnInit(): void {
		// Create the file uploader, wire it to upload to your account
		const uploaderOptions: FileUploaderOptions = {
			url: `https://api.cloudinary.com/v1_1/${this.cloudinary.config().cloud_name}/upload`,
			// Upload files automatically upon addition to upload queue
			autoUpload: false,
			// Use xhrTransport in favor of iframeTransport
			isHTML5: true,
			// Calculate progress independently for each uploaded file
			removeAfterUpload: true,
			// XHR request headers
			headers: [
				{
					name: 'X-Requested-With',
					value: 'XMLHttpRequest'
				}
			]
		};
		this.uploader = new FileUploader(uploaderOptions);

		this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
			// Add Cloudinary's unsigned upload preset to the upload form
			form.append('upload_preset', this.cloudinary.config().upload_preset);
			// Add built-in and custom tags for displaying the uploaded photo in the list
			let tags = 'rock-atlas-gallery';
			if (this.title) {
				form.append('context', `photo=${this.title}`);
				tags = `rock-atlas-gallery, ${this.title}`;
			}
			// Upload to a custom folder
			// Note that by default, when uploading via the API, folders are not automatically created in your Media Library.
			// In order to automatically create the folders based on the API requests,
			// please go to your account upload settings and set the 'Auto-create folders' option to enabled.
			form.append('folder', 'ROCK-Atlas');
			// Add custom tags
			form.append('tags', tags);
			// Add file to upload
			form.append('file', fileItem);

			// Use default "withCredentials" value for CORS requests
			fileItem.withCredentials = false;
			return { fileItem, form };
		};

		// Insert or update an entry in the responses array
		const upsertResponse = (fileItem: any) => {

			// Run the update in a custom zone since for some reason change detection isn't performed
			// as part of the XHR request to upload the files.
			// Running in a custom zone forces change detection
			this.zone.run(() => {
				// Update an existing entry if it's upload hasn't completed yet

				// Find the id of an existing item
				const existingId = this.responses.reduce((prev, current, index) => {
					if (current.file.name === fileItem.file.name && !current.status) {
						return index;
					}
					return prev;
				}, -1);
				if (existingId > -1) {
					// Update existing item with new data
					this.responses[existingId] = Object.assign(this.responses[existingId], fileItem);
				} else {
					// Create new response
					this.responses.push(fileItem);
				}
			});
		};

		// Update model on completion of uploading a file
		this.uploader.onCompleteItem = (item: any, response: string, status: number, headers: ParsedResponseHeaders) => upsertResponse({
			file: item.file,
			status,
			data: JSON.parse(response)
		});

		// Update model on upload progress event
		this.uploader.onProgressItem = (fileItem: any, progress: any) => upsertResponse({
			file: fileItem.file,
			progress,
			data: {}
		});

		this.uploader.onAfterAddingFile = (fileItem: any) => {
			fileItem.filePreviewPath  = this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(fileItem._file)));
			this.responses.push(fileItem);
			this.onAfterAddingFile.emit( fileItem );
		}

		this.uploader.onCompleteAll = () => {
			this.onUploadResult.emit({
				msg: 'all files uploaded',
				status: 200,
				files: this.responses
			});
		}
	}

	startUpload() { this.uploader.uploadAll(); }
	clearUploadQueue() {
		this.uploader.clearQueue();
		this.responses = [];
	}

	updateTitle(value: string) { this.title = value; }

	// Delete an uploaded image
	// Requires setting "Return delete token" to "Yes" in your upload preset configuration
	// See also https://support.cloudinary.com/hc/en-us/articles/202521132-How-to-delete-an-image-from-the-client-side-
	deleteImage(data: any, index: number) {
		console.log( data, index );
		// const url = `https://api.cloudinary.com/v1_1/${this.cloudinary.config().cloud_name}/delete_by_token`;
		// const headers = new Headers({ 'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest' });
		// const options = { headers: headers };
		// const body = {
		// 	token: data.delete_token
		// };

		// return this.http.post(url, body, options)
		// this.http.post(url, body, options).subscribe((response: any  ) => {
		// 	console.log(`Deleted image - ${data.public_id} ${response.result}`);
		// 	// Remove deleted item for responses
		// 	this.responses.splice(index, 1);
		// });
	};

	fileOverBase(e: any): void { this.hasBaseDropZoneOver = e; }

	getFileProperties(fileProperties: any) {
		// Transforms Javascript Object to an iterable to be used by *ngFor
		if (!fileProperties) {
			return null;
		}
		return Object.keys(fileProperties)
		.map((key) => ({ 'key': key, 'value': fileProperties[key] }));
	}
}
