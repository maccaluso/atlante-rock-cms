import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';

import * as mapboxgl from 'mapbox-gl';

import { MapService } from './map.service';

@Component({
	selector: 'map-box',
	templateUrl: './map-box.component.html',
	styleUrls: ['./map-box.component.scss']
})
export class MapBoxComponent implements OnInit {
	@Input() center: number[] = [9.172066045315233, 40.792836923962426];
	@Input() zoom: number = 3.5;
	@Input() styleUrl: string = 'mapbox://styles/mapbox/outdoors-v9';
	@Input() geocode: boolean = false;
	@Input() noEvents: boolean = false;
	@Input() containerH: string = '210px';
	@Input() markerW: string = '15px';
	@Input() markerH: string = '15px';
	@Input() markerColor: string = '#56267D';
	@Input() markerTextColor: string = '#ffffff';
	@Input() markerFontSize: string = '10px';
	@Input() markers: any;
	@Input() drawMode: string = 'point';
	@Input() areaCoords: number[][] = [];
	@Input() areaStrokeWeight: number = 5;
	@Input() areaStrokeColor: string = 'red';
	@Input() areaFillColor: string = 'red';
	@Input() areaFillOpacity: number = .5;
	@Output() onMapLoad = new EventEmitter<number[]>();
	@Output() onMapClick = new EventEmitter<any>();

	map: mapboxgl.Map;
	marker: mapboxgl.Marker;
	markerObjs: any = [];
	mapIsLoaded: boolean = false;
	defaultLocation: number[] = [9.172066045315233, 40.792836923962426];
	geocodeResults: any;

	constructor( private mapSvc: MapService ) { }

	ngOnInit() {
		this.map = new mapboxgl.Map({
			container: 'map',
			style: this.styleUrl,
			zoom: this.zoom,
			center: this.center as mapboxgl.LngLatLike || this.defaultLocation as mapboxgl.LngLatLike
		});

		this.map.getContainer().style.height = this.containerH;
		this.map.resize();
		this.map.addControl( new mapboxgl.NavigationControl() );
		
		if(!this.noEvents)
			this.initMapEvents();
		
		if( this.geocode )
			this.mapSvc.initGeocoder();
	}

	geolocateUser() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(position => {
				console.log( position.coords );
			});
		}
	}

	public forwardGeocode( address: string ) {
		return this.mapSvc.geocode( address );
	}

	public reverseGeocode2( location: number[] ) {
		return this.mapSvc.reverseGeocode( location );
	}

	setDefaultLocation() { this.map.setCenter( this.defaultLocation as mapboxgl.LngLatLike ); }

	zoomTo(level: number) { this.map.setZoom( level ); }

	getCurrentZoom() { return this.map.getZoom(); }

	flyTo(location: number[], zoom: number) {
		this.center = location;
		this.map.flyTo({
			center: location as mapboxgl.LngLatLike,
			zoom: zoom,
			speed: 3
		})
	}

	initMapEvents() {
		// console.log( 'init map events' );
		// this.map.off('click');
		this.map.on('click', (event: any) => {
			let coordinates = [event.lngLat.lng, event.lngLat.lat];
			switch(this.drawMode) {
				case 'point':
					this.drawPoint( coordinates );
					break;
				case 'multiPoints':
					this.addMultiPoint( coordinates );
					break;
				case 'areaStroke':
					this.addPointToArea( coordinates, 'areaStroke' );
					break;
				case 'areaFill':
					this.addPointToArea( coordinates, 'areaFill' );
					break;
			}
		});

		// this.map.off('load');
		this.map.on('load', (event: any) => {
			this.mapIsLoaded = true; 
			this.flyTo( this.center, this.zoom );

			switch( this.drawMode ) {
				case 'point':
					this.addPointMarker( this.center || this.defaultLocation );
					break;
				case 'multiPoints':
					this.markers.forEach(marker => {
						//console.log(marker.location.longitude, marker.location.latitude);
						this.addPointMarker( [marker.location.longitude, marker.location.latitude] );
					});
					break;
				case 'areaStroke':
					if( this.areaCoords.length > 2 )
						this.addArea();
					break;
				case 'areaFill':
					if( this.areaCoords.length > 2 )
						this.addArea();
					break;
			}
			
			this.onMapLoad.emit( this.center );
		});
	}

	drawPoint(coords: number[]) {
		if(!this.marker)
			this.addPointMarker( coords );
		else
			this.marker.setLngLat( coords as mapboxgl.LngLatLike );

		this.onMapClick.emit({type: 'point', data: coords});
	}

	addMultiPoint(coords: number[]) {
		this.markers.push({
			location: {
				longitude: coords[0],
				latitude: coords[1]
			}
		});

		this.renderMarkers( this.markers );
		this.onMapClick.emit({type: 'multiPoints', data: this.markers});
	}

	addPointMarker(coords: number[]) {
		this.removePointMarker();

		let el = document.createElement('div');
		el.className = 'marker';
		el.setAttribute('style', `
			width: ${this.markerW}; 
			height: ${this.markerH}; 
			background-color: ${this.markerColor};
			border-radius: 50%;
		`);

		this.marker = new mapboxgl.Marker(el)
			.setLngLat(coords as mapboxgl.LngLatLike)
			.addTo(this.map);
	}

	removePointMarker() { if(this.marker) { this.marker.remove(); } }

	renderMarkers(markers: any) {
		for(let i = 0; i < markers.length; i++)
		{
			let coords = [ markers[i].location.longitude, markers[i].location.latitude ];

			let el = document.createElement('div');
			el.className = 'marker';
			el.setAttribute('style', `
				width: ${this.markerW}; 
				height: ${this.markerH}; 
				background-color: ${this.markerColor};
				border-radius: 50%;
				color: ${this.markerTextColor};
				font-size: ${this.markerFontSize};
				display: flex;
				align-items: center;
				justify-content: center;
			`);
			el.innerText = '' + (i+1);

			let marker = new mapboxgl.Marker(el);
			marker.setLngLat( coords as mapboxgl.LngLatLike ).addTo( this.map );

			el.addEventListener('click', (e) => {
				e.preventDefault();
				e.stopPropagation();
				
				this.markers.splice( i, 1 );
				this.markerObjs[ i ].remove();

				this.renderMarkers( this.markers );
			});

			this.markerObjs.push( marker );
		}
	}

	addArea() {
		// console.log( 'Add area' );
		if(!this.map.getSource('areaSource'))
		{
			this.map.addSource('areaSource', { type: 'geojson', data: {
				"type": "Feature",
				"properties": {},
				"geometry": {
					"type": "LineString",
					"coordinates": this.areaCoords
				}
			}});
		}

		if(!this.map.getLayer('area'))
		{
			if(this.drawMode == 'areaFill')
			{
				this.map.addLayer({
					"id": "area",
					"type": "fill",
					"source": 'areaSource',
					"paint": { 
						"fill-color": this.areaFillColor,
						"fill-opacity": this.areaFillOpacity
					}
				});
			}

			if(this.drawMode == 'areaStroke')
			{
				this.map.addLayer({
					"id": "area",
					"type": "line",
					"source": 'areaSource',
					"layout": {
						"line-join": "round",
						"line-cap": "round"
					},
					"paint": { 
						"line-color": this.areaStrokeColor, 
						"line-width": this.areaStrokeWeight 
					}
				});
			}			
		}

		this.updateArea();
	}

	addPointToArea(coords: number[], type: string) {
		this.areaCoords.push( coords );

		if( !this.map.getLayer('area') )
			this.addArea();

		this.updateArea();

		this.onMapClick.emit({type: type, data: this.areaCoords});
	}

	updateArea() {
		// console.log( 'Update area' );
		let src = this.map.getSource('areaSource') as any;
		src.setData({
			"type": "Feature",
			"properties": {},
			"geometry": {
				"type": "LineString",
				"coordinates": this.areaCoords
			}
		});
	}

	removeArea() { 
		// console.log( 'Remove area' );
		if( this.map.getLayer('area') )
			this.map.removeLayer('area')
		if( this.map.getSource('areaSource') )
			this.map.removeSource('areaSource');
	}

	switchAreaType() {
		// console.log( 'Switch area type' );
		this.removePointMarker();
		this.removeArea();
		this.addArea();
	}

	removeLastPoint() {
		this.areaCoords.pop();
		this.updateArea();
	}

	closeAreaPolygon() {
		let first = this.areaCoords[0];
		let last = this.areaCoords[ this.areaCoords.length - 1 ];

		if((first[0] != last[0]) && (first[1] != last[1]))
		{
			this.areaCoords.push( first );
			this.updateArea();
		}
	}
}