import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';

import { FirebaseService } from '../../services/firebase.service';

import * as mapboxgl from 'mapbox-gl';
import { Observable } from 'rxjs';

@Component({
  selector: 'map-box-draw',
  templateUrl: './map-box-draw.component.html',
  styleUrls: ['./map-box-draw.component.scss']
})
export class MapBoxDrawComponent implements OnInit {
  @Input() styleUrl = 'mapbox://styles/mapbox/outdoors-v9';
  @Input() zoom: number;
  @Input() containerH = '210px';
  @Input() markerW = '15px';
  @Input() markerH = '15px';
  @Input() markerColor = '#56267D';
  @Input() markerTextColor = '#ffffff';
  @Input() markerFontSize = '8px';
  @Input() areaStrokeWeight = 5;
  @Input() areaStrokeColor = '#56267D';
  @Input() areaFillColor = '#56267D';
  @Input() areaFillOpacity = .5;
  @Output() onMapLoad = new EventEmitter<string>();
  @Output() onMapClick = new EventEmitter<number[]>();
  @Output() onSelectedMarkersRemove = new EventEmitter<number[]>();
  @Output() onMarkerRemoveAll = new EventEmitter<boolean>();
  @Output() onAreaUpdate = new EventEmitter<any>();
  @Output() onAreaDelete = new EventEmitter<boolean>();

  private map: mapboxgl.Map;
  private defaultLocation: number[];
  private center: number[];
  private defaultZoom: number;
  private currentZoom: number;

  public drawMode: string;
  private markers: mapboxgl.Marker[];
  private areaCoords: any;

  private existingActions: Observable<any>;
  private existingContexts: Observable<any>;

  constructor(private firebase: FirebaseService, ) {
    this.defaultLocation = [9.172066045315233, 40.792836923962426];
    this.defaultZoom = 3.5;
  }

  ngOnInit() {
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.styleUrl,
      zoom: this.defaultZoom,
      center: this.defaultLocation as mapboxgl.LngLatLike
    });

    this.map.getCanvasContainer().style.cursor = 'default';

    this.map.getContainer().style.height = this.containerH;
    this.map.resize();
    this.map.addControl( new mapboxgl.NavigationControl() );

    this.map.on('load', () => {
      this.onMapLoad.emit( 'map loaded' );

      this.existingActions = this.firebase.getList('/actions');
      this.existingActions.subscribe(
        (actions: any) => {
          const features = [];

          for (let i = 0; i < actions.length; i++) {
            features.push({
              'type': 'Feature',
              'geometry': {
                'type': 'Point',
                'coordinates': [actions[i].geolocation.lng, actions[i].geolocation.lat]
              },
              'properties': {
                'title': actions[i].name,
                'icon': 'circle'
              }
            });
          }

          if (this.map.getLayer('actions')) {
            this.map.removeLayer('actions');
          }
          if (this.map.getSource('actions')) {
            this.map.removeSource('actions');
          }
          this.map.addLayer({
            'id': 'actions',
            'type': 'symbol',
            'source': {
              'type': 'geojson',
              'data': {
                'type': 'FeatureCollection',
                'features': features
              }
            },
            'layout': {
              'icon-image': '{icon}-11',
              'text-field': '{title}',
              'text-size': 12,
              'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
              'text-offset': [0, 0.6],
              'text-anchor': 'top'
            }
          });
        },
        (err: any) => console.log(err)
      );
    });

    this.map.on('click', (evt: any) => {
      this.onMapClick.emit( evt.lngLat );
    });
  }

  public getDrawMode() {
    return this.drawMode;
  }

  public setDrawMode(mode: string) {
    this.drawMode = mode;
    this.deleteAllMarkers();

    if ( (this.drawMode === 'areaFill' || this.drawMode === 'areaStroke') && this.areaCoords ) {
      this.initArea( this.areaCoords );
    }
  }

  public centerMap( coords: number[], destZoom: number ) {
    console.log( 'center map' );

    this.center = coords;
    this.currentZoom = destZoom;

    this.map.setCenter( this.center as mapboxgl.LngLatLike );
    this.map.setZoom( this.currentZoom );
  }

  public renderMarker( coords: number[], index: number = 0 ) {
    // console.log( 'render marker' );

    const el = document.createElement('div');
    el.innerHTML = '' + ( index + 1 );
    el.className = 'marker';
    el.id = 'marker-' + index;
    el.setAttribute('style', `
      width: ${this.markerW};
      height: ${this.markerH};
      background-color: ${this.markerColor};
      color: ${this.markerTextColor};
      font-size: ${this.markerFontSize};
      border-radius: 50%;
    `);

    el.addEventListener('click', (e: MouseEvent) => {
      e.preventDefault();
      e.stopImmediatePropagation();
      e.stopPropagation();

      el.classList.contains('selected') === false ? el.classList.add('selected') : el.classList.remove('selected');
    });

    const m = new mapboxgl.Marker( el )
      .setLngLat( coords as mapboxgl.LngLatLike )
      .addTo( this.map);

    if (this.markers) {
      this.markers.push( m );
    } else {
      this.markers = [ m ];
    }
  }

  public moveMarker( coords: number[], index: number ) {
    if ( this.markers && this.markers.length > 0 ) {
      this.markers[index].setLngLat( coords as mapboxgl.LngLatLike );
    } else {
      this.renderMarker( coords, index );
    }
  }

  private deleteAllMarkers() {
    if ( this.markers && this.markers.length > 0 ) {
      this.markers.forEach((marker) => {
        marker.remove();
      });
      this.markers = [];
      this.onMarkerRemoveAll.emit( true );
    }
  }

  deleteSelectedMarkers() {
    if ( this.markers && this.markers.length > 0 ) {
      const removedIndexes = [];
      this.markers.forEach((marker, index) => {
        if ( marker.getElement().classList.contains('selected') ) {
          removedIndexes.push( index );
        }
        marker.remove();
      });

      this.markers = [];
      this.onSelectedMarkersRemove.emit( removedIndexes );
    }
  }

  public initArea( coords: any ) {
    this.areaCoords = coords;

    if ( this.map.getLayer('area') ) {
      this.map.removeLayer('area');
    }
    if ( this.map.getSource('areaSource') ) {
      this.map.removeSource('areaSource');
    }

    this.map.addSource('areaSource', { type: 'geojson', data: {
      'type': 'Feature',
      'properties': {},
      'geometry': {
        'type': 'LineString',
        'coordinates': coords
      }
    }});

    if (this.drawMode === 'areaFill') {
      this.map.addLayer({
        'id': 'area',
        'type': 'fill',
        'source': 'areaSource',
        'paint': {
          'fill-color': this.areaFillColor,
          'fill-opacity': this.areaFillOpacity
        }
      });
    }

    if (this.drawMode === 'areaStroke') {
      this.map.addLayer({
        'id': 'area',
        'type': 'line',
        'source': 'areaSource',
        'layout': {
          'line-join': 'round',
          'line-cap': 'round'
        },
        'paint': {
          'line-color': this.areaStrokeColor,
          'line-width': this.areaStrokeWeight
        }
      });
    }
  }

  public updateArea( coords ) {
    this.areaCoords = coords;

    const src = this.map.getSource('areaSource') as any;
    src.setData({
      'type': 'Feature',
      'properties': {},
      'geometry': {
        'type': 'LineString',
        'coordinates': coords
      }
    });

    this.onAreaUpdate.emit( coords );
  }

  deleteArea() {
    this.areaCoords = [];
    this.updateArea( this.areaCoords );
    this.onAreaDelete.emit( true );
  }

  deleteLastVertex() {
    if ( this.areaCoords.length > 0 ) {
      this.areaCoords.pop();
      this.updateArea( this.areaCoords );
    }
  }

  closeArea() {
    const first = this.areaCoords[0];
    const last = this.areaCoords[ this.areaCoords.length - 1 ];

    if ((first[0] !== last[0]) && (first[1] !== last[1])) {
      this.areaCoords.push( first );
      this.updateArea( this.areaCoords );
    }
  }

  toggleFeatures() {
    const visibility = this.map.getLayoutProperty('actions', 'visibility');

    if (visibility === 'visible') {
      this.map.setLayoutProperty('actions', 'visibility', 'none');
    } else {
      this.map.setLayoutProperty('actions', 'visibility', 'visible');
    }
  }
}
