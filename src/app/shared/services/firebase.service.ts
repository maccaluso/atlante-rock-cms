import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  public listSize: number = 0;

  constructor(
    private db: AngularFireDatabase
  ) { }

  getList(path: string) {
    return this.db.list( path ).snapshotChanges().pipe(
      map(changes => 
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  getListSize(path: string) {
    this.db.list( path ).valueChanges().subscribe(
      (data) => this.listSize = data.length
    );
  }

  getFilteredList(path: string, order: string, match: any) {
    return this.db.list( path, ref => ref.orderByChild( order ).equalTo( match ) ).snapshotChanges().pipe(
      map(changes => 
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  getOrderedList(path: string, order: string) {
    return this.db.list( path, ref => ref.orderByChild( order ) ).snapshotChanges().pipe(
      map(changes => 
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  getPaginatedList(path: string, order: string, start: number, limit: number) {
    return this.db.list( path, ref => ref.orderByChild( order ).startAt( start ).limitToFirst( limit ) ).snapshotChanges().pipe(
      map(changes => 
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  getObject(path: string) {
    return this.db.object(path).snapshotChanges().pipe(
      map(
        (changes) => ({ key: changes.payload.key, ...changes.payload.val() })
      )
    );
  }

  updateObject(path: string, payload: any) {
    return this.db.object( path ).update( payload );
  }

  setObjectWithKey(path: string, value: any) {
    return this.db.object( path ).set( value )
  }

  createObject(path: string, payload: any) {
    return this.db.list( path ).push( payload );
  }

  deleteObject(path: string, key: string) {
    return this.db.list( path ).remove( key );
  }
}
