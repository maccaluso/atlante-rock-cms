import { Injectable } from '@angular/core';

import { 
    faBell as bellFull,
    faBookmark as bookmarkFull,
    faCircle as circleFull, 
    faDotCircle as dotCircleFull,
    faGem as gemFull,
    faHeart as heartFull,
    faLemon as lemonFull,
    faLifeRing as liferingFull,
    faLightbulb as lightbulbFull,
    faMinusSquare as minusSquareFull,
    faMoon as moonFull,
    faPlusSquare as plusSquareFull,
    faSquare as squareFull,
    faStar as starFull,
    faStopCircle as stopCircleFull
} from '@fortawesome/free-solid-svg-icons';

import { 
    faBell as bellEmpty,
    faBookmark as bookmarkEmpty,
    faCircle as circleEmpty, 
    faDotCircle as dotCircleEmpty,
    faGem as gemEmpty,
    faHeart as heartEmpty,
    faLemon as lemonEmpty,
    faLifeRing as liferingEmpty,
    faLightbulb as lightbulbEmpty,
    faMinusSquare as minusSquareEmpty,
    faMoon as moonEmpty,
    faPlusSquare as plusSquareEmpty,
    faSquare as squareEmpty,
    faStar as starEmpty,
    faStopCircle as stopCircleEmpty
} from '@fortawesome/free-regular-svg-icons';

@Injectable()
export class SymbolsService {

    private symbols: any;
    private icons: any;
    private shapes: any;

	constructor() {
        this.symbols = [
            {
                name: 'BLACK SQUARE',
                dec: '&#9632;',
                hex: '&#x25A0;',
                empty: {
                    name: 'WHITE SQUARE',
                    dec: '&#9633;',
                    hex: '&#x25A1;'
                }
            },
            {
                name: 'BLACK CIRCLE',
                dec: '&#9679;',
                hex: '&#x25CF;',
                empty: {
                    name: 'WHITE CIRCLE',
                    dec: '&#9675;',
                    hex: '&#x25CB;'
                }
            },
            {
                name: 'BLACK UP-POINTING TRIANGLE',
                dec: '&#9650;',
                hex: '&#x25B2;',
                empty: {
                    name: 'WHITE UP-POINTING TRIANGLE',
                    dec: '&#9651;',
                    hex: '&#x25B3;'
                }
            },
            {
                name: 'BLACK DIAMOND',
                dec: '&#9670;',
                hex: '&#x25C6;',
                empty: {
                    name: 'WHITE DIAMOND',
                    dec: '&#9671;',
                    hex: '&#x25C7;'
                }
            }
        ];

        this.icons = [
            {
                name: 'bell',
                full: bellFull,
                empty: bellEmpty
            },
            {
                name: 'bookmark',
                full: bookmarkFull,
                empty: bookmarkEmpty
            },
            {
                name: 'circle',
                full: circleFull,
                empty: circleEmpty
            },
            {
                name: 'dot circle',
                full: dotCircleFull,
                empty: dotCircleEmpty
            },
            {
                name: 'gem',
                full: gemFull,
                empty: gemEmpty
            },
            {
                name: 'heart',
                full: heartFull,
                empty: heartEmpty
            },
            {
                name: 'lemon',
                full: lemonFull,
                empty: lemonEmpty
            },
            {
                name: 'life ring',
                full: liferingFull,
                empty: liferingEmpty
            },
            {
                name: 'light bulb',
                full: lightbulbFull,
                empty: lightbulbEmpty
            },
            {
                name: 'minus square',
                full: minusSquareFull,
                empty: minusSquareEmpty
            },
            {
                name: 'moon',
                full: moonFull,
                empty: moonEmpty
            },
            {
                name: 'plus square',
                full: plusSquareFull,
                empty: plusSquareEmpty
            },
            {
                name: 'square',
                full: squareFull,
                empty: squareEmpty
            },
            {
                name: 'star',
                full: starFull,
                empty: starEmpty
            },
            {
                name: 'stop circle',
                full: stopCircleFull,
                empty: stopCircleEmpty
            }
        ]
    
        this.shapes = [
            'square',
            'circle',
            'rhombus',
            'hexagon',
            'triangle'
        ]
    }

	getSymbols() { return this.symbols; }
    getIcons() { return this.icons; }
    getShapes() { return this.shapes; }

    getSymbolByName(name: string) {
        return 'get symbol by name: ' + name;
    }
}