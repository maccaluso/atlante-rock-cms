import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { NotifyService } from './notify.service';

import { User } from '../interfaces/User';

import { Observable } from 'rxjs';
// import { of, empty } from "rxjs";
// import { switchMap } from 'rxjs/operators/';

@Injectable()
export class AuthService {

    user: Observable<User>;

    constructor(
        private afAuth: AngularFireAuth,
        private afs: AngularFirestore,
        private router: Router,
        private notify: NotifyService
    ) {

        this.user = this.afAuth.authState;
            // .pipe(
            // 	switchMap(
            // 		(user) => {
            // 			console.log( user );
            // 			if (user)
            // 			{
            // 				return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
            // 			} else {
            // 				return of( new Observable<User>(null) );
            // 			}
            // 		}
            // 	)
            // );
    }

    ////// OAuth Methods /////

    // facebookLogin() {
    //     const provider = new firebase.auth.FacebookAuthProvider();
    //     return this.oAuthLogin(provider);
    // }

    // private oAuthLogin(provider: firebase.auth.AuthProvider) {
    //     return this.afAuth.auth.signInWithPopup(provider)
    //     .then((credential) => {
    //         this.notify.update('You\'re logged in', 'Welcome to ROCK Atlas CMS', 'success');
    //         return this.updateUserData(credential.user);
    //     })
    //     .catch((error) => this.handleError(error) );
    // }

    //// Email/Password Auth ////

    emailLogin(email: string, password: string) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    }

    // Sends email allowing user to reset password
    resetPassword(email: string) {
        const fbAuth = firebase.auth();

        return fbAuth.sendPasswordResetEmail(email);
        // .then(() => this.notify.update('Password update email sent', 'info'))
        // .catch((error) => this.handleError(error));
    }

    signOut() {
        return this.afAuth.auth.signOut();
    }

    // If error, console log and notify user
    private handleError(error: Error) {
        console.error(error);
        this.notify.update('Something went wrong!', error.message, 'error');
    }

    // Sets user data to firestore after succesful login
    // private updateUserData(user: User) {

    //     // const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);

    //     // const data: User = {
    //     // 	uid: user.uid,
    //     // 	email: user.email || null,
    //     // 	displayName: user.displayName || 'nameless user',
    //     // 	photoURL: user.photoURL || 'https://goo.gl/Fz9nrQ',
    //     // };
    //     // return userRef.set(data);
    // }
}
