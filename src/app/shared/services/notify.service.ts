import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';

/// Notify users about errors and other helpful stuff
export interface Msg {
  header: string;
  content: string;
  style: string;
}

@Injectable()
export class NotifyService {

  private _msgSource = new Subject<Msg | null>();

  msg = this._msgSource.asObservable();

  update(header: string, content: string, style: 'error' | 'info' | 'success' | 'warning') {
    const msg: Msg = { header, content, style };
    this._msgSource.next(msg);

    let tm = setTimeout((e) => {
      this.clear();
      clearTimeout( tm );
    }, 3000);
  }

  clear() {
    this._msgSource.next(null);
  }
}
