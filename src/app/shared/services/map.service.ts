import { Injectable } from '@angular/core';
import { from } from "rxjs";

import { environment } from '../../../environments/environment';

// import { GeoJson } from './map';
import * as mapboxgl from 'mapbox-gl';
import * as MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import centroid from "@turf/centroid";

@Injectable()
export class MapService {
	geocoder: any;

	constructor() {
		try {
			(mapboxgl as any).accessToken = environment.mapbox.accessToken;
		} catch(err) {
			console.warn(err);
		}
	}

	initGeocoder() {
		console.log( 'init geocoder' );
		// this.client = new MapboxClient( mapboxgl.accessToken );
		this.geocoder = new MapboxGeocoder({
			accessToken: mapboxgl.accessToken
		});
	}

	geocode(address: string) {
		return from(
			this.geocoder.geocodeForward( address )
		)
	}

	reverseGeocode(location: number[]) {
		return from(
			this.geocoder.geocodeReverse({
				longitude: location[0],
				latitude: location[1]
			})
		)
	}

	getCentroid(points: any) {
		let poly = {
			"type": "Feature",
			"properties": {},
			"geometry": {
				"type": "Polygon",
				"coordinates": points
			}
		};
	
		return centroid( poly ).geometry.coordinates;
	}
}