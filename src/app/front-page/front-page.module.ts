import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { FrontPageComponent } from './front-page.component';

import { AuthService } from '../shared/services/auth.service';
import { NotifyService } from '../shared/services/notify.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule
  ],
  declarations: [
    FrontPageComponent
  ],
  providers: [
    AuthService,
    NotifyService
  ]
})
export class FrontPageModule { }
