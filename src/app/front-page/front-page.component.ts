import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { faFileExport, faAngleDown, faSearch } from '@fortawesome/free-solid-svg-icons';
import { saveAs } from 'file-saver';

import { AuthService } from '../shared/services/auth.service';
import { FirebaseService } from '../shared/services/firebase.service';

@Component({
  selector: 'app-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.scss']
})
export class FrontPageComponent implements OnInit {
  private dbCities: Observable<any>;
  private cities: any;
  private filteredCities: any;
  private dbCategories: Observable<any>;
  private categories: any;
  private filteredCategories: any;
  private dbTypes: Observable<any>;
  private types: any;
  private filteredTypes: any;
  private dbContexts: Observable<any>;
  private contexts: any;
  private filteredContexts: any;
  private dbActions: Observable<any>;
  private actions: any;
  private filteredActions: any;
  private dbMeta: Observable<any>;
  private meta: any;
  private filteredMeta: any;

  private faFileExport = faFileExport;
  private faAngleDown = faAngleDown;
  private faSearch = faSearch;

  private ddState = { cities: false, categories: false, types: false, contexts: false, actions: false, meta: false };

  constructor(
    public auth: AuthService,
    public firebase: FirebaseService
  ) { }

  ngOnInit() {
    this.dbCities = this.firebase.getList('/cities');
    this.dbCities.subscribe(
      (data) => this.filteredCities = this.cities = data,
      (err) => console.log(err)
    );

    this.dbCategories = this.firebase.getList('/rock-categories');
    this.dbCategories.subscribe(
      (data) => this.categories = this.filteredCategories = data,
      (err) => console.log(err)
    );

    this.dbTypes = this.firebase.getList('/rock-category-types');
    this.dbTypes.subscribe(
      (data) => this.types = this.filteredTypes = data,
      (err) => console.log(err)
    );

    this.dbContexts = this.firebase.getList('/cultural-contexts');
    this.dbContexts.subscribe(
      (data) => this.contexts = this.filteredContexts = data,
      (err) => console.log(err)
    );

    this.dbActions = this.firebase.getList('/actions');
    this.dbActions.subscribe(
      (data) => this.actions = this.filteredActions = data,
      (err) => console.log(err)
    );

    this.dbMeta = this.firebase.getList('/meta');
    this.dbMeta.subscribe(
      (data) => this.meta = this.filteredMeta = data,
      (err) => console.log(err)
    );
  }

  toggleDropDown(type: string) {
    this.ddState[type] = !this.ddState[type];
  }

  onSearchChange(search: any, type: string) {
    const originalType = type.replace('filtered', '').toLowerCase();

    if (search === '') {
      this[type] = this[originalType];
    } else {
      this[type] = this[originalType].filter(
        (item: any) => item.name.toLowerCase().indexOf(search.toLowerCase()) > -1
      );
    }
  }

  exportDB() {
    this.actions.map(
      (action: any) => {
        this.meta.map(
          (meta: any) => {
            if (meta.key === action.key) {
              action.meta = meta;
            }
          }
        );
      }
    );

    const data = {
      cities: this.cities,
      categories: this.categories,
      types: this.types,
      contexts: this.contexts,
      actions: this.actions
    };
    const fileName = 'atlasDB.json';

    const fileToSave = new Blob([JSON.stringify(data, undefined, 2)], {
      type: 'application/json'
    });

    saveAs(fileToSave, fileName);
  }

}
