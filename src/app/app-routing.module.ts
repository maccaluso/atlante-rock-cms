import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './shared/services/auth.guard';

import { FrontPageComponent } from './front-page/front-page.component';
import { UserLoginComponent } from './user-login/user-login.component';

import { CityListComponent } from './cities/city-list/city-list.component';
import { CityFormComponent } from './cities/city-form/city-form.component';

import { CategoryListComponent } from './categories/category-list/category-list.component';
import { CategoryFormComponent } from './categories/category-form/category-form.component';

import { TypeListComponent } from './types/type-list/type-list.component';
import { TypeFormComponent } from './types/type-form/type-form.component';

import { ContextListComponent } from './contexts/context-list/context-list.component';
import { ContextFormComponent } from './contexts/context-form/context-form.component';

import { ActionListComponent } from './actions/action-list/action-list.component';
import { ActionFormComponent } from './actions/action-form/action-form.component';

import { PracticeListComponent } from './practices/practice-list/practice-list.component';
import { PracticeFormComponent } from './practices/practice-form/practice-form.component';

const routes: Routes = [
  { path: '', component: FrontPageComponent },
  { path: 'login', component: UserLoginComponent },

  { path: 'cities', component: CityListComponent, canActivate: [AuthGuard] },
  { path: 'cities/new', component: CityFormComponent, canActivate: [AuthGuard] },
  { path: 'cities/edit/:id', component: CityFormComponent, canActivate: [AuthGuard] },

  { path: 'rock-categories', component: CategoryListComponent, canActivate: [AuthGuard] },
  { path: 'rock-categories/new', component: CategoryFormComponent, canActivate: [AuthGuard] },
  { path: 'rock-categories/edit/:id', component: CategoryFormComponent, canActivate: [AuthGuard] },

  { path: 'rock-category-types', component: TypeListComponent, canActivate: [AuthGuard] },
  { path: 'rock-category-types/new', component: TypeFormComponent, canActivate: [AuthGuard] },
  { path: 'rock-category-types/edit/:id', component: TypeFormComponent, canActivate: [AuthGuard] },

  { path: 'contexts', component: ContextListComponent, canActivate: [AuthGuard] },
  { path: 'contexts/new', component: ContextFormComponent, canActivate: [AuthGuard] },
  { path: 'contexts/edit/:id', component: ContextFormComponent, canActivate: [AuthGuard] },

  { path: 'actions', component: ActionListComponent, canActivate: [AuthGuard] },
  { path: 'actions/new', component: ActionFormComponent, canActivate: [AuthGuard] },
  { path: 'actions/edit/:id', component: ActionFormComponent, canActivate: [AuthGuard] },

  { path: 'model-practices', component: PracticeListComponent, canActivate: [AuthGuard] },
  { path: 'model-practices/new', component: PracticeFormComponent, canActivate: [AuthGuard] },
  { path: 'model-practices/edit/:id', component: PracticeFormComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
