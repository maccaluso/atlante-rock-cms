import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';

import { environment } from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';

import { AppComponent } from './app.component';

import { WidgetsModule } from './shared/widgets/widgets.module';

import { FrontPageModule } from './front-page/front-page.module';
import { UserLoginModule } from './user-login/user-login.module';

import { CitiesModule } from './cities/cities.module';
import { CategoriesModule } from './categories/categories.module';
import { TypesModule } from './types/types.module';
import { ContextModule } from './contexts/contexts.module';
import { ActionModule } from './actions/actions.module';
import { PracticeModule } from './practices/practice.module';

import { AuthGuard } from './shared/services/auth.guard';

import { MapService } from './shared/widgets/map-box/map.service';
import { MapService as MapService2 } from './shared/services/map.service';

import { SlugifyService } from './shared/services/slugify.service';
import { NotifyService } from './shared/services/notify.service';
import { SymbolsService } from './shared/services/symbols.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    CloudinaryModule.forRoot({Cloudinary}, environment.cloudinary as CloudinaryConfiguration),
    WidgetsModule,
    FrontPageModule,
    UserLoginModule,
    CitiesModule,
    CategoriesModule,
    TypesModule,
    ContextModule,
    ActionModule,
    PracticeModule
  ],
  providers: [
    AuthGuard,
    MapService,
    MapService2,
    SlugifyService,
    NotifyService,
    SymbolsService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
