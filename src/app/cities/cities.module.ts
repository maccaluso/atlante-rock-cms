import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from "@angular/forms";

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { FileUploadModule } from 'ng2-file-upload';
import { WidgetsModule } from '../shared/widgets/widgets.module';

import { CityListComponent } from './city-list/city-list.component';
import { CityFormComponent } from './city-form/city-form.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    CloudinaryModule,
    FileUploadModule,
    WidgetsModule
  ],
  declarations: [
    CityListComponent, 
    CityFormComponent
  ]
})
export class CitiesModule { }