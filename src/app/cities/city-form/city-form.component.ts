import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";
import { Observable } from 'rxjs';

import { faBan, faCheck, faUpload } from '@fortawesome/free-solid-svg-icons';

import { FormUploaderComponent } from '../../shared/widgets/form-uploader/form-uploader.component';
import { FirebaseService } from "../../shared/services/firebase.service";
import { SlugifyService } from "../../shared/services/slugify.service";
import { NotifyService } from "../../shared/services/notify.service";
import { MapBoxComponent } from '../../shared/widgets/map-box/map-box.component';

@Component({
  selector: 'app-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.scss']
})
export class CityFormComponent implements OnInit {
  @ViewChild( MapBoxComponent ) map: MapBoxComponent;
  @ViewChild( FormUploaderComponent ) uploader: FormUploaderComponent;

  city: Observable<any>;
  location: number[];
  objectKey: string;
  isNewItem: boolean = true;
  itemSingular: string = 'city';
  itemPlural: string = 'cities';
  isSaving = false;

  cityForm: FormGroup;
  formErrors = {
    'name': '',
    'slug': '',
    'type': '',
    'geolocation': '',
    'description': '',
    'population': '',
    'images': '',
    'videos': '',	
    'active': '',
    'created': '',
    'lastUpdated': '',
    'author': ''
  };
  validationMessages = {
    'name': {
      'required': 'Name is required.'
    },
    'slug': {
      'required': 'Slug is required.'
    },
    'type': {
      'required': 'Type is required.'
    },
  };

  faCheck = faCheck;
  faBan = faBan;
  faUpload = faUpload;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    this.cityForm = this.fb.group({
      name: [ '', Validators.required ],
      slug: [ '', Validators.required ],
      type: [ '', Validators.required ],
      geolocation: { lng: '', lat: '' },
      description: '',
      population: '',
      images: [],
      videos: [],	
      active: true,
      created: '',
      lastUpdated: '',
      author: {}
    });      

    this.cityForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged();

    this.route.params.subscribe(
      (data) => {
        if(data.id) {
          this.isNewItem = false;

          this.city = this.firebase.getObject('/cities/' + data.id)
          this.city.subscribe(
            (data) => {
              this.objectKey = data.key;
              if( data.geolocation )
                this.location = [ data.geolocation.lng, data.geolocation.lat ];

              this.cityForm.setValue({
                name: data.name ? data.name : null,
                slug: data.slug ? data.slug : null,
                type: data.type ? data.type : null,
                geolocation: data.geolocation ? data.geolocation : { lng: '', lat: '' },
                description: data.description ? data.description : null,
                population: data.population ? data.population : null,
                images: data.images ? data.images : [],
                videos: data.videos ? data.videos : [],	
                active: data.active ? data.active : true,
                created: data.created ? data.created : Date.now(),
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now(),
                author: data.author ? data.author : {}
              });
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.cityForm.get('geolocation').patchValue({ lng: this.map.defaultLocation[0], lat: this.map.defaultLocation[1] });
    });
  }

  onValueChanged(data?: any) {    
    if (!this.cityForm) { return; }
    
    const form = this.cityForm;

    if(data)
      form.get('slug').patchValue( this.slugifySvc.slugify( data.name ), { emitEvent: false } );

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        // clear previous error message (if any)
        this.formErrors[field] = '';

        const control = form.get(field);
        
        if (control && control.touched && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  onMapLoad(evt: any) {}

  onMapClick(evt: any) {
    this.cityForm.patchValue({
      geolocation: { lng: evt.data[0], lat: evt.data[1] }
    });
    this.location = [ evt.data[0], evt.data[1] ];
  }

  onUploadResult(evt: any) {
    this.uploader.clearUploadQueue();

		if( evt.status == 200 ) {
      this.notify.update('Keep going!', 'Done uploading', 'info');

			for (var i = 0; i < evt.files.length; ++i) {
        if( evt.files[i].data.resource_type == 'image' )
        {
          let imgs = this.cityForm.get('images').value;
          imgs.push( evt.files[i].data );
          this.cityForm.get('images').patchValue( imgs );
        }

        if( evt.files[i].data.resource_type == 'video' )
        {
          let videos = this.cityForm.get('videos').value;
          videos.push( evt.files[i].data );
          this.cityForm.get('videos').patchValue( videos );
        }
			}

			this.save();
		}
  }

  onAfterAddingFile(evt: any) {
    // console.log( evt );
  }

  deleteImage(index: number) {
    let imgs = this.cityForm.get('images').value;
    imgs.splice( index, 1 );
    this.cityForm.get('images').patchValue( imgs );
  }

  deleteVideo(index: number) {
    let videos = this.cityForm.get('videos').value;
    videos.splice( index, 1 );
    this.cityForm.get('videos').patchValue( videos );
  }

  save() {
    if( this.uploader.responses.length > 0)
		{
      this.uploader.startUpload();
      return;
    }
    
    this.isSaving = true;

    this.cityForm.patchValue({ lastUpdated: Date.now() });

    if( this.isNewItem )
    {
      this.firebase.createObject( `cities`,  this.cityForm.getRawValue() ).then(
        () => {
          this.notify.update('Great!', 'Item created', 'success');
          this.isSaving = false;
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
    else
    {
      console.log( this.cityForm.getRawValue() );
      this.firebase.updateObject( `cities/${ this.objectKey }`,  this.cityForm.getRawValue() ).then(
        () => {
          this.isSaving = false;
          this.notify.update('Great!', 'Item updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }

}
