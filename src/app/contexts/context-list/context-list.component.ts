import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { faPlus, faCheckCircle, faBan, faTimes } from '@fortawesome/free-solid-svg-icons';

import { NotifyService } from '../../shared/services/notify.service';
import { FirebaseService } from '../../shared/services/firebase.service';

@Component({
  selector: 'context-list',
  templateUrl: './context-list.component.html',
  styleUrls: ['./context-list.component.scss']
})
export class ContextListComponent implements OnInit {

  debug = false;

  contextSubject: Subject<any>;
  contexts: any;
  queryObservable: Observable<any>;

  catObs: Observable<any>;
  categories: any;

  cityObs: Observable<any>;
  cities: any;

  showSpinner = true;
  isDeletingItem = false;
  itemToDelete: number;
  isSavingOrder = false;

  currentPage = 0;
  itemsPerPage = 1000;
  numItems: number;
  itemDragged: any;
  disablePrevPage = true;
  disableNextPage = false;

  categoryFilter: string;
  cityFilter: string;
  nameFilter: string;
  paginationEnabled: boolean;

  faPlus = faPlus;
  faCheckCircle = faCheckCircle;
  faBan = faBan;
  faTimes = faTimes;

  constructor(
    private firebase: FirebaseService,
    private notify: NotifyService
  ) {
    this.categoryFilter = 'none';
    this.cityFilter = 'none';
    this.nameFilter = '';
    this.paginationEnabled = true;
    this.contextSubject = new Subject<string>();
  }

  ngOnInit() {
    this.catObs = this.firebase.getList('/rock-categories');
    this.catObs.subscribe( cats => this.categories = cats );

    this.cityObs = this.firebase.getList('/cities');
    this.cityObs.subscribe( cities => this.cities = cities );

    this.firebase.getListSize('/cultural-contexts');

    this.queryObservable = this.contextSubject.pipe(
      switchMap(
        (params) => {
          // console.log(params);
          if ( params.match ) {
            return this.firebase.getFilteredList( params.path, params.orderBy, params.match );
          } else {
            return this.firebase.getPaginatedList( params.path, params.orderBy, params.startAt * this.itemsPerPage, this.itemsPerPage );
          }
        }
      )
    );

    this.queryObservable.subscribe(
      (queriedItems) => {
        // console.log(queriedItems);
        this.contexts = queriedItems;
        this.showSpinner = false;
      },
      (err) => console.log(err),
      () => console.log('complete')
    );

    this.contextSubject.next({
      path: '/cultural-contexts',
      startAt: this.currentPage, 
      orderBy: 'order'
    });
  }

  prevPage() {
    if( this.currentPage == 0 ) {
      this.disablePrevPage = true;
      this.disableNextPage = false;
      return;
    } else {
      this.currentPage--;
      if( this.currentPage == 0 ) {
        this.disablePrevPage = true;
        this.disableNextPage = false;
      }

      this.contextSubject.next({
        path: '/cultural-contexts',
        startAt: this.currentPage, 
        orderBy: 'order'
      });
    }
  }

  nextPage() {
    let numPages = Math.ceil( this.firebase.listSize / this.itemsPerPage );

    this.currentPage++;
    
    if( this.currentPage < numPages -1 ) {
      this.disablePrevPage = false;
    }
    
    if( this.currentPage == numPages - 1 ) {
      this.disableNextPage = true;
    }

    if( this.currentPage > numPages - 1 ) {
      this.currentPage = numPages - 1;
      return;
    }

    this.contextSubject.next({
      path: '/cultural-contexts',
      startAt: this.currentPage, 
      orderBy: 'order'
    });
  }

  onItemsPerPageChange($event) { 
    this.currentPage = 0;
    this.resetNameFilter();

    this.itemsPerPage = parseInt($event);

    this.contextSubject.next({
      path: '/cultural-contexts',
      startAt: this.currentPage, 
      orderBy: 'order'
    }); 
  }

  onNameFilterChange($event) {
    this.nameFilter = $event;
    this.paginationEnabled = false;

    if( $event == '' ) {
      this.paginationEnabled = true;
    }

    this.contexts.map(
      (context) => {
        if( context.name.toLowerCase().indexOf( $event.toLowerCase() ) > -1 ) {
          context.hidden = false;
        } else {
          context.hidden = true;
        }
      }
    );
  }

  resetNameFilter() {
    this.onNameFilterChange('');
  }

  onCategoryFilterChange($event) {
    this.categoryFilter = $event;
    this.cityFilter = 'none';
    this.resetNameFilter();
    this.currentPage = 0;
    this.paginationEnabled = false;

    let params: any = {
      path: '/cultural-contexts',
      orderBy: 'ROCKcategory',
      match: $event
    };

    if($event == 'none') {
      this.paginationEnabled = true;

      params = {
        path: '/cultural-contexts',
        orderBy: 'order',
        startAt: this.currentPage
      };
    }

    this.contextSubject.next( params );
  }

  onCityFilterChange($event) {
    this.cityFilter = $event;
    this.categoryFilter = 'none';
    this.resetNameFilter();
    this.currentPage = 0;
    this.paginationEnabled = false;

    let params: any = {
      path: '/cultural-contexts',
      orderBy: 'city',
      match: $event
    };

    if($event == 'none') {
      this.paginationEnabled = true;

      params = {
        path: '/cultural-contexts',
        orderBy: 'order',
        startAt: this.currentPage
      };
    }

    this.contextSubject.next( params );
  }

  onDragStart(evt: any) {
    // console.log( 'start', evt );
    this.itemDragged = evt;
  }

  onDrop(evt: any) {
    // console.log( this.itemDragged.order, evt.order );
    // this.resetItemsOrder();
    // this.saveAll();

    // this.contextSubject.next( this.currentPage );
  }

  getCatName(catID: string) {
    let catName;
    this.categories.forEach(cat => {
      if( cat.key == catID ) {
        catName = cat.name;
      }
    });
    return catName;
  }

  getCityName(cityID: string) {
    let cityName;
    this.cities.forEach(city => {
      if( city.key == cityID ) {
        cityName = city.name;
      }
    });
    return cityName;
  }

  getContextType(type: string) {
    let typeString;
    switch( type ) {
      case 'point':
        typeString = 'Point';
        break;
      case 'areaFill':
        typeString = 'Fill';
        break;
      case 'areaStroke':
        typeString = 'Stroke';
        break;
    }
    return typeString;
  }

  resetTypes() {
    this.contexts.forEach(context => {
      if( context.type == 'area' ) {
        context.type = 'areaStroke';
      }
    });
  }

  resetItemsOrder() {
    this.contexts.forEach((context, i) => {
      context.order = i;
    });
  }

  resetCity() {
    this.contexts.forEach((context, i) => {
      context.city = '-LAE2GG1STdas-LE1U5e';
    });
  }

  saveAll() {
    this.isSavingOrder = true;
    let counter = 0;

    this.contexts.forEach((context, i) => {
      this.firebase.updateObject('cultural-contexts/' + context.key, context).then(
        () => {
          counter++;
          if( counter == this.contexts.length ) {
            this.notify.update('Great!', 'Items order updated', 'success');
            this.isSavingOrder = false;
          }
        },
        (err) => console.log(err)
      );
    });
  }

  deleteItem(key: string, index: number) {
    this.isDeletingItem = true;
    this.itemToDelete = index;
    this.firebase.deleteObject('cultural-contexts', key).then(
      () => {
        this.isDeletingItem = false;
        this.notify.update('Done!', 'Item deleted', 'success');
      },
      (err) => console.log( err )
    );
  }
}