import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { FileUploadModule } from 'ng2-file-upload';
import { DndModule } from 'ng2-dnd';

import { WidgetsModule } from '../shared/widgets/widgets.module';

import { ContextListComponent } from './context-list/context-list.component';
import { ContextFormComponent } from './context-form/context-form.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    CloudinaryModule,
    FileUploadModule,
    DndModule.forRoot(),
    WidgetsModule
  ],
  declarations: [
    ContextListComponent, 
    ContextFormComponent
  ]
})
export class ContextModule { }