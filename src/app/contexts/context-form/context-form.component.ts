import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { faBan, faCheck, faUpload } from '@fortawesome/free-solid-svg-icons';

import { FormUploaderComponent } from '../../shared/widgets/form-uploader/form-uploader.component';
import { FirebaseService } from '../../shared/services/firebase.service';
import { SlugifyService } from '../../shared/services/slugify.service';
import { NotifyService } from '../../shared/services/notify.service';
// import { MapBoxComponent } from '../../shared/widgets/map-box/map-box.component';

import { MapBoxDrawComponent } from '../../shared/widgets/map-box-draw/map-box-draw.component';
import { MapService } from '../../shared/services/map.service';

@Component({
  selector: 'context-form',
  templateUrl: './context-form.component.html',
  styleUrls: ['./context-form.component.scss']
})
export class ContextFormComponent implements OnInit {
  @ViewChild( MapBoxDrawComponent ) map: MapBoxDrawComponent;
  @ViewChild( FormUploaderComponent ) uploader: FormUploaderComponent;

  debug = false;

  context: Observable<any>;
  cities: Observable<any>;
  cityData: any;
  categories: Observable<any>;
  location: number[];
  objectKey: string;
  isNewItem = true;
  itemSingular = 'context item';
  itemPlural = 'context items';
  isSaving = false;
  geoMode = 'point';
  showDescriptionField = false;

  contextForm: FormGroup;
  formErrors = {
    'name': '',
    'slug': '',
    'type': '',
    'geolocation': '',
    'description': '',
    'images': '',
    'videos': '',
    'active': '',
    'created': '',
    'lastUpdated': '',
    'author': '',
    'city': '',
    'ROCKcategory': ''
  };
  validationMessages = {
    'name': {
      'required': 'Name is required.'
    },
    'slug': {
      'required': 'Slug is required.'
    },
    'type': {
      'required': 'Type is required.'
    },
  };

  faCheck = faCheck;
  faBan = faBan;
  faUpload = faUpload;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService,
    private mapSvc: MapService
  ) { }

  ngOnInit() {
    this.contextForm = this.fb.group({
      ROCKcategory: {},
      active: true,
      area: [],
      city: {},
      created: '',
      description: '',
      geolocation: [ { lng: '', lat: '' }, Validators.required ],
      images: [],
      lastUpdated: '',
      name: [ '', Validators.required ],
      order: 0,
      slug: [ '', Validators.required ],
      summary: '',
      type: [ '', Validators.required ],
      videos: []
    });

    this.contextForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.contextForm.get('name').valueChanges.subscribe((data) => this.onNameChange(data));
    this.contextForm.get('city').valueChanges.subscribe((data) => this.onCityChange(data));
    this.contextForm.get('type').valueChanges.subscribe((data) => this.onGeoModeChange(data));

    this.route.params.subscribe(
      (data) => {
        if (data.id) {
          this.isNewItem = false;

          this.context = this.firebase.getObject('/cultural-contexts/' + data.id);
          this.context.subscribe(
            (data) => {
              this.objectKey = data.key;
              if ( data.geolocation ) {
                this.location = [ data.geolocation.lng, data.geolocation.lat ];
              }

              this.contextForm.setValue({
                ROCKcategory: data.ROCKcategory ? data.ROCKcategory : null,
                active: data.active ? data.active : true,
                area: data.area ? data.area : [],
                city: data.city ? data.city : {},
                created: data.created ? data.created : Date.now(),
                description: data.description ? data.description : null,
                geolocation: data.geolocation ? data.geolocation : { lng: '', lat: '' },
                images: data.images ? data.images : [],
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now(),
                name: data.name ? data.name : null,
                order: data.order ? data.order : 0,
                slug: data.slug ? data.slug : null,
                summary: data.summary ? data.summary : null,
                type: data.type ? data.type : this.geoMode,
                videos: data.videos ? data.videos : []
              });
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );

    this.cities = this.firebase.getFilteredList('cities', 'type', 'replicator');
    this.cities.subscribe(
      (data) => {
        this.cityData = data;
      }
    );

    this.categories = this.firebase.getList('rock-categories');
  }

  mapLoaded(evt: any) {
    switch ( this.map.getDrawMode() ) {
      case 'point':
        const geolocation = this.contextForm.get('geolocation').value;
        if ( geolocation && geolocation.lng && geolocation.lat ) {
          this.map.centerMap( [ geolocation.lng, geolocation.lat ], 15 );
          this.map.renderMarker( [ geolocation.lng, geolocation.lat ], 0 );
        } else {
          this.reachCity();
        }
        break;
      case 'multiPoints':
        const points = this.contextForm.get('points').value;
        if ( points && points.length > 0 ) {
          // const points = this.contextForm.get('points').value;
          points.forEach((point, index) => {
            this.map.renderMarker( point, index );
          });
          this.map.centerMap( this.mapSvc.getCentroid( [points] ), 13 );
        } else {
          this.reachCity();
        }
        break;
      case 'areaStroke':
        const coords = this.contextForm.get('area').value;
        if ( coords.length > 0 ) {
          this.map.initArea( coords );
          this.map.centerMap( this.mapSvc.getCentroid( [coords] ), 13 );
        } else {
          this.reachCity();
        }
        break;
      case 'areaFill':
        const coords2 = this.contextForm.get('area').value;
        if ( coords2.length > 0 ) {
          this.map.initArea( coords2 );
          this.map.centerMap( this.mapSvc.getCentroid( [coords2] ), 13 );
        } else {
          this.reachCity();
        }
        break;
    }
  }

  mapClicked(evt: any) {
    const coords = [ evt.lng, evt.lat ];

    switch ( this.map.getDrawMode() ) {
      case 'point':
        this.contextForm.get('geolocation').patchValue( evt );
        this.map.moveMarker( coords, 0 );
        break;
      case 'multiPoints':
        const points = this.contextForm.get('points').value || [];
        points.push( coords );
        this.contextForm.get('points').patchValue( points );
        this.map.renderMarker( coords, points.length - 1 );
        break;
      case 'areaStroke':
        const area = this.contextForm.get('area').value || [];
        area.push( coords );
        this.contextForm.get('area').patchValue( area );

        if ( area.length === 1 ) {
          this.map.initArea( area );
        } else {
          this.map.updateArea( area );
        }
        break;
      case 'areaFill':
        const area2 = this.contextForm.get('area').value || [];
        area2.push( coords );
        this.contextForm.get('area').patchValue( area2 );

        if ( area2.length === 1 ) {
          this.map.initArea( area2 );
        } else {
          this.map.updateArea( area2 );
        }
        break;
    }
  }

  selectedMarkersRemoved(indexes: number[]) {
    if ( this.map.getDrawMode() === 'point' ) {
      this.contextForm.get('geolocation').patchValue({});
      this.reachCity();
    }

    if ( this.map.getDrawMode() === 'multiPoints' ) {
      if ( indexes.length === 0 ) {
        return;
      }

      const points = this.contextForm.get('points').value;

      indexes.forEach(index => {
        points.splice(index, 1);
      });

      this.contextForm.get('points').patchValue( points );
      points.forEach((point, index) => {
        this.map.renderMarker( point, index );
      });

      if ( points.length === 1 ) {
        this.map.centerMap( points[0], 13 );
      }

      if ( points.length > 1 ) {
        this.map.centerMap( this.mapSvc.getCentroid( [points] ), 13 );
      }
    }
  }

  markerAllRemoved(evt: boolean) {
    this.contextForm.get('points').patchValue( [] );
  }

  areaDeleted(evt: boolean) {
    this.contextForm.get('area').patchValue([]);
  }

  toggleDescriptionField() {
    this.showDescriptionField = !this.showDescriptionField;
  }

  onValueChanged(data?: any) {
    if (!this.contextForm) { return; }

    const form = this.contextForm;

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        // clear previous error message (if any)
        this.formErrors[field] = '';

        const control = form.get(field);

        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  onNameChange(data: any) {
    this.contextForm.get('slug').patchValue( this.slugifySvc.slugify( data ), { emitEvent: false } );
  }

  onCityChange(data?: any) {
    this.reachCity();
  }

  reachCity() {
    if ( this.cityData ) {
      this.cityData.forEach(city => {
        if ( city.key == this.contextForm.get('city').value ) {
          this.map.centerMap( [ city.geolocation.lng, city.geolocation.lat ], 10 );
        }
      });
    }
  }

  onGeoModeChange(data?: any) {
    if ( data !== this.map.getDrawMode() ) {
      this.map.setDrawMode( data );
    }
  }

  onUploadResult(evt: any) {
    this.uploader.clearUploadQueue();

    if ( evt.status === 200 ) {
      this.notify.update('Keep going!', 'Done uploading', 'info');

      for (let i = 0; i < evt.files.length; ++i) {
        if ( evt.files[i].data.resource_type === 'image' ) {
          const imgs = this.contextForm.get('images').value;
          imgs.push( evt.files[i].data );
          this.contextForm.get('images').patchValue( imgs );
        }

        if ( evt.files[i].data.resource_type === 'video' ) {
          const videos = this.contextForm.get('videos').value;
          videos.push( evt.files[i].data );
          this.contextForm.get('videos').patchValue( videos );
        }
      }

      this.save();
    }
  }

  onAfterAddingFile(evt: any) {
    // console.log( evt );
  }

  deleteImage(index: number) {
    const imgs = this.contextForm.get('images').value;
    imgs.splice( index, 1 );
    this.contextForm.get('images').patchValue( imgs );
  }

  deleteVideo(index: number) {
    const videos = this.contextForm.get('videos').value;
    videos.splice( index, 1 );
    this.contextForm.get('videos').patchValue( videos );
  }

  save() {
    if ( this.uploader.responses.length > 0) {
      this.uploader.startUpload();
      return;
    }

    this.isSaving = true;

    this.contextForm.patchValue({ lastUpdated: Date.now() });

    if ( this.isNewItem ) {
      this.firebase.createObject( `cultural-contexts`,  this.contextForm.getRawValue() ).then(
        (data: any) => {
          this.notify.update('Great!', 'Item created', 'success');
          this.isSaving = false;
          this.router.navigate(['/contexts/edit/' + data.key]);
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    } else {
      this.firebase.updateObject( `cultural-contexts/${ this.objectKey }`,  this.contextForm.getRawValue() ).then(
        () => {
          this.isSaving = false;
          this.notify.update('Great!', 'Item updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }

}

