import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { faPlus, faCheckCircle, faBan, faTimes } from '@fortawesome/free-solid-svg-icons';

import { NotifyService } from '../../shared/services/notify.service';
import { FirebaseService } from '../../shared/services/firebase.service';

@Component({
  selector: 'action-list',
  templateUrl: './action-list.component.html',
  styleUrls: ['./action-list.component.scss']
})
export class ActionListComponent implements OnInit {

  debug = false;

  contextSubject: Subject<any>;
  contexts: any;
  queryObservable: Observable<any>;

  catObs: Observable<any>;
  categories: any;

  typeObs: Observable<any>;
  types: any;

  cityObs: Observable<any>;
  cities: any;

  showSpinner = true;
  isDeletingItem = false;
  itemToDelete: number;
  isSavingOrder = false;

  currentPage = 0;
  itemsPerPage = 1000;
  numItems: number;
  itemDragged: any;
  disablePrevPage = true;
  disableNextPage = false;

  categoryFilter: string;
  typeFilter: string;
  cityFilter: string;
  nameFilter: string;
  paginationEnabled: boolean;

  faPlus = faPlus;
  faCheckCircle = faCheckCircle;
  faBan = faBan;
  faTimes = faTimes;

  isMetadataModalOpen = false;
  rockPartners = [
    {
      id: 1,
      slug: 'acciona',
      label: 'ACCIONA'
    },
    {
      id: 2,
      slug: 'addma',
      label: 'ADDMA'
    },
    {
      id: 3,
      slug: 'af-ukim',
      label: 'AF-UKIM'
    },
    {
      id: 5,
      slug: 'asfa',
      label: 'ASFA'
    },
    {
      id: 6,
      slug: 'cluj',
      label: 'Cluj'
    },
    {
      id: 7,
      slug: 'cobo',
      label: 'COBO'
    },
    {
      id: 8,
      slug: 'conf',
      label: 'CONF'
    },
    {
      id: 9,
      slug: 'corv',
      label: 'CORV'
    },
    {
      id: 10,
      slug: 'eco4clim',
      label: 'ECO4CLIM'
    },
    {
      id: 11,
      slug: 'eindh',
      label: 'Eindh'
    },
    {
      id: 12,
      slug: 'eurocities',
      label: 'Eurocities'
    },
    {
      id: 13,
      slug: 'fitz',
      label: 'FITZ'
    },
    {
      id: 14,
      slug: 'fiu',
      label: 'FIU'
    },
    {
      id: 15,
      slug: 'iclei',
      label: 'Iclei'
    },
    {
      id: 16,
      slug: 'icsul',
      label: 'ICSUL'
    },
    {
      id: 17,
      slug: 'jb',
      label: 'JB'
    },
    {
      id: 18,
      slug: 'lisbon',
      label: 'Lisbon'
    },
    {
      id: 19,
      slug: 'liverpool',
      label: 'Liverpool'
    },
    {
      id: 20,
      slug: 'luci',
      label: 'LUCI'
    },
    {
      id: 21,
      slug: 'lyon',
      label: 'Lyon'
    },
    {
      id: 22,
      slug: 'now',
      label: 'NOW'
    },
    {
      id: 23,
      slug: 'skopje',
      label: 'Skopje'
    },
    {
      id: 24,
      slug: 'taso',
      label: 'TASO'
    },
    {
      id: 25,
      slug: 'tue',
      label: 'TUe'
    },
    {
      id: 26,
      slug: 'turin',
      label: 'Turin'
    },
    {
      id: 27,
      slug: 'ucm',
      label: 'UCM'
    },
    {
      id: 28,
      slug: 'unibo',
      label: 'UNIBO'
    },
    {
      id: 29,
      slug: 'uniyork',
      label: 'UniYork'
    },
    {
      id: 30,
      slug: 'urbalyon',
      label: 'UrbaLyon'
    },
    {
      id: 31,
      slug: 'urbaso',
      label: 'URBASO'
    },
    {
      id: 32,
      slug: 'vgtu',
      label: 'VGTU'
    },
    {
      id: 33,
      slug: 'viabizzuno',
      label: 'VIABIZZUNO'
    },
    {
      id: 34,
      slug: 'vilnius',
      label: 'VILNIUS'
    },
    {
      id: 35,
      slug: 'vwg',
      label: 'VWG'
    },
    {
      id: 36,
      slug: 'vwlabs',
      label: 'VWLabs'
    }
  ];
  rockScenarios = [
    {
      id: 1,
      slug: 'accessibility',
      label: 'accessibility'
    },
    {
      id: 2,
      slug: 'sustainability',
      label: 'sustainability'
    },
    {
      id: 3,
      slug: 'collaboration',
      label: 'collaboration'
    }
  ];
  actionMeta: any = {};
  currentMetaKey: string;
  currentActionMeta: Observable<any>;

  constructor(
    private firebase: FirebaseService,
    private notify: NotifyService
  ) {
    this.categoryFilter = 'none';
    this.typeFilter = 'none';
    this.cityFilter = 'none';
    this.nameFilter = '';
    this.paginationEnabled = true;
    this.contextSubject = new Subject<string>();
  }

  ngOnInit() {
    this.catObs = this.firebase.getList('/rock-categories');
    this.catObs.subscribe( cats => this.categories = cats );

    this.typeObs = this.firebase.getList('/rock-category-types');
    this.typeObs.subscribe( types => this.types = types );

    this.cityObs = this.firebase.getList('/cities');
    this.cityObs.subscribe( cities => this.cities = cities );

    this.firebase.getListSize('/actions');

    this.queryObservable = this.contextSubject.pipe(
      switchMap(
        (params) => {
          // console.log(params);
          if ( params.match ) {
            return this.firebase.getFilteredList( params.path, params.orderBy, params.match );
          } else {
            return this.firebase.getPaginatedList( params.path, params.orderBy, params.startAt * this.itemsPerPage, this.itemsPerPage );
          }
        }
      )
    );

    this.queryObservable.subscribe(
      (queriedItems) => {
        // console.log(queriedItems);
        this.contexts = queriedItems;
        this.showSpinner = false;
      },
      (err) => console.log(err),
      () => console.log('complete')
    );

    this.contextSubject.next({
      path: '/actions',
      startAt: this.currentPage,
      orderBy: 'order'
    });
  }

  prevPage() {
    if ( this.currentPage === 0 ) {
      this.disablePrevPage = true;
      this.disableNextPage = false;
      return;
    } else {
      this.currentPage--;
      if ( this.currentPage === 0 ) {
        this.disablePrevPage = true;
        this.disableNextPage = false;
      }

      this.contextSubject.next({
        path: '/actions',
        startAt: this.currentPage,
        orderBy: 'order'
      });
    }
  }

  nextPage() {
    const numPages = Math.ceil( this.firebase.listSize / this.itemsPerPage );

    this.currentPage++;

    if ( this.currentPage < numPages - 1 ) {
      this.disablePrevPage = false;
    }

    if ( this.currentPage == numPages - 1 ) {
      this.disableNextPage = true;
    }

    if ( this.currentPage > numPages - 1 ) {
      this.currentPage = numPages - 1;
      return;
    }

    this.contextSubject.next({
      path: '/actions',
      startAt: this.currentPage,
      orderBy: 'order'
    });
  }

  onItemsPerPageChange($event) {
    this.currentPage = 0;
    this.resetNameFilter();

    this.itemsPerPage = parseInt($event);

    this.contextSubject.next({
      path: '/actions',
      startAt: this.currentPage,
      orderBy: 'order'
    });
  }

  onNameFilterChange($event) {
    this.nameFilter = $event;
    this.paginationEnabled = false;

    if ( $event === '' ) {
      this.paginationEnabled = true;
    }

    this.contexts.map(
      (context) => {
        if ( context.name.toLowerCase().indexOf( $event.toLowerCase() ) > -1 ) {
          context.hidden = false;
        } else {
          context.hidden = true;
        }
      }
    );
  }

  resetNameFilter() {
    this.onNameFilterChange('');
  }

  onCategoryFilterChange($event) {
    this.categoryFilter = $event;
    this.cityFilter = 'none';
    this.typeFilter = 'none';
    this.resetNameFilter();
    this.currentPage = 0;
    this.paginationEnabled = false;

    let params: any = {
      path: '/actions',
      orderBy: 'ROCKcategory',
      match: $event
    };

    if ($event === 'none') {
      this.paginationEnabled = true;

      params = {
        path: '/actions',
        orderBy: 'order',
        startAt: this.currentPage
      };
    }

    this.contextSubject.next( params );
  }

  onTypeFilterChange($event) {
    this.typeFilter = $event;
    this.cityFilter = 'none';
    this.categoryFilter = 'none';
    this.resetNameFilter();
    this.currentPage = 0;
    this.paginationEnabled = false;

    let params: any = {
      path: '/actions',
      orderBy: 'ROCKtype',
      match: $event
    };

    if ($event === 'none') {
      this.paginationEnabled = true;

      params = {
        path: '/actions',
        orderBy: 'order',
        startAt: this.currentPage
      };
    }

    this.contextSubject.next( params );
  }

  onCityFilterChange($event) {
    this.cityFilter = $event;
    this.categoryFilter = 'none';
    this.typeFilter = 'none';
    this.resetNameFilter();
    this.currentPage = 0;
    this.paginationEnabled = false;

    let params: any = {
      path: '/actions',
      orderBy: 'city',
      match: $event
    };

    if ($event == 'none') {
      this.paginationEnabled = true;

      params = {
        path: '/actions',
        orderBy: 'order',
        startAt: this.currentPage
      };
    }

    this.contextSubject.next( params );
  }

  onDragStart(evt: any, index: number) {
    this.itemDragged = evt;
  }

  onDrop(evt: any) {
    // this.resetItemsOrder();
    // this.saveAll();
    // this.contextSubject.next( this.currentPage );
  }

  getCatName(catID: string) {
    let catName: any;
    this.categories.forEach(cat => {
      if ( cat.key === catID ) {
        catName = cat.name;
      }
    });
    return catName;
  }

  getTypeName(typeID: string) {
    let typeName: any;
    if (this.types) {
      this.types.forEach(type => {
        if ( type.key === typeID ) {
          typeName = type.name;
        }
      });
    }
    return typeName;
  }

  getCityName(cityID: string) {
    let cityName: any;
    if (this.cities) {
      this.cities.forEach(city => {
        if ( city.key === cityID ) {
          cityName = city.name;
        }
      });
    }
    return cityName;
  }

  getContextType(type: string) {
    let typeString;
    switch ( type ) {
      case 'point':
        typeString = 'Point';
        break;
      case 'multiPoints':
        typeString = 'Multiple points';
        break;
      case 'areaFill':
        typeString = 'Fill';
        break;
      case 'areaStroke':
        typeString = 'Stroke';
        break;
    }
    return typeString;
  }

  resetTypes() {
    this.contexts.forEach(context => {
      if ( context.type === 'area' ) {
        context.type = 'areaStroke';
      }
    });
  }

  resetItemsOrder() {
    this.contexts.forEach((context, i) => {
      context.order = i;
    });
  }

  resetCity() {
    this.contexts.forEach((context, i) => {
      context.city = '-LAE2GG1STdas-LE1U5e';
    });
  }

  hasLongForm(blocks: any) {
    if ( blocks && blocks.length > 0 ) {
      return true;
    } else {
      return false;
    }
  }

  saveAll() {
    this.isSavingOrder = true;
    let counter = 0;

    this.contexts.forEach((context, i) => {
      this.firebase.updateObject('actions/' + context.key, context).then(
        () => {
          counter++;
          if ( counter === this.contexts.length ) {
            this.notify.update('Great!', 'Items order updated', 'success');
            this.isSavingOrder = false;
          }
        },
        (err) => console.log(err)
      );
    });
  }

  deleteItem(key: string, index: number) {
    this.isDeletingItem = true;
    this.itemToDelete = index;
    this.firebase.deleteObject('actions', key).then(
      () => {
        this.isDeletingItem = false;
        this.notify.update('Done!', 'Item deleted', 'success');
      },
      (err) => console.log( err )
    );
  }

  editMetaData(action: any) {
    this.currentMetaKey = action.key;

    this.currentActionMeta = this.firebase.getObject('/meta/' + this.currentMetaKey);
    this.currentActionMeta.subscribe(
      (data) => {
        this.actionMeta = data;
        this.isMetadataModalOpen = true;
      },
      (err) => this.notify.update('Error', err, 'error')
    );
  }

  closeMetadataModal() {
    this.isMetadataModalOpen = false;
  }

  cancelMetaEdit() {
    this.actionMeta = {};
    this.currentMetaKey = '';
    this.closeMetadataModal();
  }

  saveActionMeta() {
    this.firebase.updateObject('/meta/' + this.currentMetaKey, this.actionMeta).then(
      () => this.notify.update('Success', 'Great! Action metadata updated', 'success'),
      (err) => this.notify.update('Error', err, 'error')
    );
  }
}
