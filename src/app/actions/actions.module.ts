import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { FileUploadModule } from 'ng2-file-upload';
import { DndModule } from 'ng2-dnd';
import { DpDatePickerModule } from 'ng2-date-picker';
import { AngularEditorModule } from '@kolkov/angular-editor';

import { WidgetsModule } from '../shared/widgets/widgets.module';

import { ActionListComponent } from './action-list/action-list.component';
import { ActionFormComponent } from './action-form/action-form.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule,
    CloudinaryModule,
    FileUploadModule,
    DndModule.forRoot(),
    DpDatePickerModule,
    AngularEditorModule,
    WidgetsModule
  ],
  declarations: [
    ActionListComponent, 
    ActionFormComponent
  ]
})
export class ActionModule { }