import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { faBan, faCheck, faUpload } from '@fortawesome/free-solid-svg-icons';
import { AngularEditorConfig } from '@kolkov/angular-editor';

import { FormUploaderComponent } from '../../shared/widgets/form-uploader/form-uploader.component';
import { FormUploaderButtonComponent } from '../../shared/widgets/form-uploader-button/form-uploader-button.component';
import { FirebaseService } from '../../shared/services/firebase.service';
import { SlugifyService } from '../../shared/services/slugify.service';
import { NotifyService } from '../../shared/services/notify.service';

import { MapBoxDrawComponent } from '../../shared/widgets/map-box-draw/map-box-draw.component';
import { MapService } from '../../shared/services/map.service';

@Component({
  selector: 'action-form',
  templateUrl: './action-form.component.html',
  styleUrls: ['./action-form.component.scss']
})
export class ActionFormComponent implements OnInit {
  @ViewChild( MapBoxDrawComponent ) map: MapBoxDrawComponent;
  @ViewChild( FormUploaderComponent ) uploader: FormUploaderComponent;
  @ViewChild( FormUploaderButtonComponent ) uploaderButton: FormUploaderButtonComponent;

  debug = false;

  context: Observable<any>;
  cities: Observable<any>;
  cityData: any;
  categories: Observable<any>;
  actions: Observable<any>;
  practices: Observable<any>;
  tags: Observable<any>;
  categoryTypes: any;
  objectKey: string;
  isNewItem = true;
  itemSingular = 'ROCK action';
  itemPlural = 'ROCK actions';
  isSaving = false;
  isUploadingBlock = false;
  storyModalOpen = false;
  showDescriptionField = false;

  contextForm: FormGroup;
  formErrors = {
    'name': '',
    'slug': '',
    'type': '',
    'geolocation': '',
    'description': '',
    'images': '',
    'videos': '',
    'active': '',
    'created': '',
    'lastUpdated': '',
    'author': '',
    'city': '',
    'ROCKcategory': ''
  };
  validationMessages = {
    'name': {
      'required': 'Name is required.'
    },
    'slug': {
      'required': 'Slug is required.'
    },
    'type': {
      'required': 'Type is required.'
    },
  };

  dpConfig: any = {
    drops: 'down',
    attachTo: '.control'
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    placeholder: 'Type a paragraph...',
    translate: 'no'
  };

  faCheck = faCheck;
  faBan = faBan;
  faUpload = faUpload;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService,
    private router: Router,
    private mapSvc: MapService
  ) { }

  ngOnInit() {
    this.contextForm = this.fb.group({
      address: this.fb.group({
        country: '',
        locality: '',
        street: '',
        zipcode: ''
      }),
      active: true,
      area: [],
      blocks: [],
      city: {},
      created: '',
      description: '',
      endDate: '',
      geolocation: [ { lng: '', lat: '' }, Validators.required ],
      images: [],
      lastUpdated: '',
      links: [],
      newLink: this.fb.group({
        label: '',
        url: ''
      }),
      name: [ '', Validators.required ],
      order: 0,
      points: [],
      relatedActions: [],
      relatedPractices: [],
      ROCKcategory: {},
      ROCKtype: {},
      slug: [ '', Validators.required ],
      stakeholders: [],
      newStakeholder: this.fb.group({
        name: '',
        url: ''
      }),
      startDate: '',
      status: '',
      substatus: '',
      summary: '',
      tags: [],
      newTag: '',
      type: [ '', Validators.required ],
      videos: []
    });

    this.contextForm.valueChanges.subscribe((data) => this.onValueChanged());
    this.contextForm.get('name').valueChanges.subscribe( (data) => this.onNameChange(data) );
    this.contextForm.get('city').valueChanges.subscribe( (data) => this.onCityChange(data) );
    this.contextForm.get('type').valueChanges.subscribe( (data) => this.onGeoModeChange(data) );
    this.contextForm.get('ROCKcategory').valueChanges.subscribe( (data) => this.onCategoryChange(data) );

    this.route.params.subscribe(
      (data) => {
        if (data.id) {
          this.isNewItem = false;

          this.context = this.firebase.getObject('/actions/' + data.id);
          this.context.subscribe(
            (data) => {
              this.objectKey = data.key;

              this.contextForm.setValue({
                address: data.address ? data.address : {
                  country: '',
                  locality: '',
                  street: '',
                  zipcode: ''
                },
                active: data.active ? data.active : true,
                area: data.area ? data.area : [],
                blocks: data.blocks ? data.blocks : [],
                points: data.points ? data.points : [],
                city: data.city ? data.city : '',
                created: data.created ? data.created : Date.now(),
                description: data.description ? data.description : null,
                endDate: data.endDate ? data.endDate : '',
                geolocation: data.geolocation ? data.geolocation : { lng: '', lat: '' },
                images: data.images ? data.images : [],
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now(),
                links: data.links ? data.links : [],
                newLink: {
                  label: '',
                  url: ''
                },
                name: data.name ? data.name : null,
                order: data.order ? data.order : 0,
                relatedActions: data.relatedActions ? data.relatedActions : [],
                relatedPractices: data.relatedPractices ? data.relatedPractices : [],
                ROCKcategory: data.ROCKcategory ? data.ROCKcategory : null,
                ROCKtype: data.ROCKtype ? data.ROCKtype : null,
                slug: data.slug ? data.slug : null,
                stakeholders: data.stakeholders ? data.stakeholders : [],
                newStakeholder: {
                  name: '',
                  url: ''
                },
                startDate: data.startDate ? data.startDate : '',
                status: data.status ? data.status : null,
                substatus: data.substatus ? data.substatus : null,
                summary: data.summary ? data.summary : null,
                tags: data.tags ? data.tags : [],
                newTag: '',
                type: data.type ? data.type : '',
                videos: data.videos ? data.videos : []
              });
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );

    this.cities = this.firebase.getFilteredList('cities', 'type', 'replicator');
    this.cities.subscribe(
      (data) => {
        this.cityData = data;
      }
    );

    this.categories = this.firebase.getList('rock-categories');
    this.categories.subscribe(
      (data) => {
        data.forEach(cat => {
          if ( cat.key == this.contextForm.get('ROCKcategory').value ) {
            this.categoryTypes = cat.types;
          }
        });
      }
    );

    this.actions = this.firebase.getList('/actions');
    this.practices = this.firebase.getList('/best-practices');
    this.tags = this.firebase.getList('/tags');
  }

  mapLoaded(evt: any) {
    switch ( this.map.getDrawMode() ) {
      case 'point':
        const geolocation = this.contextForm.get('geolocation').value;
        if ( geolocation && geolocation.lng && geolocation.lat ) {
          this.map.centerMap( [ geolocation.lng, geolocation.lat ], 15 );
          this.map.renderMarker( [ geolocation.lng, geolocation.lat ], 0 );
        } else {
          this.reachCity();
        }
        break;
      case 'multiPoints':
        const points = this.contextForm.get('points').value;
        if ( points && points.length > 0 ) {
          const points = this.contextForm.get('points').value;
          points.forEach((point, index) => {
            this.map.renderMarker( point, index );
          });
          this.map.centerMap( this.mapSvc.getCentroid( [points] ), 13 );
        } else {
          this.reachCity();
        }
        break;
      case 'areaStroke':
        const coords = this.contextForm.get('area').value;
        if ( coords.length > 0 ) {
          this.map.initArea( coords );
          this.map.centerMap( this.mapSvc.getCentroid( [coords] ), 13 );
        } else {
          this.reachCity();
        }
        break;
      case 'areaFill':
        const coords2 = this.contextForm.get('area').value;
        if ( coords2.length > 0 ) {
          this.map.initArea( coords2 );
          this.map.centerMap( this.mapSvc.getCentroid( [coords2] ), 13 );
        } else {
          this.reachCity();
        }
        break;
    }
  }

  mapClicked(evt: any) {
    const coords = [ evt.lng, evt.lat ];

    switch ( this.map.getDrawMode() ) {
      case 'point':
        this.contextForm.get('geolocation').patchValue( evt );
        this.map.moveMarker( coords, 0 );
        break;
      case 'multiPoints':
        const points = this.contextForm.get('points').value || [];
        points.push( coords );
        this.contextForm.get('points').patchValue( points );
        this.map.renderMarker( coords, points.length - 1 );
        break;
      case 'areaStroke':
        const area = this.contextForm.get('area').value || [];
        area.push( coords );
        this.contextForm.get('area').patchValue( area );

        if ( area.length == 1 ) {
          this.map.initArea( area );
        } else {
          this.map.updateArea( area );
        }
        break;
      case 'areaFill':
        const area2 = this.contextForm.get('area').value || [];
        area2.push( coords );
        this.contextForm.get('area').patchValue( area2 );

        if ( area2.length == 1 ) {
          this.map.initArea( area2 );
        } else {
          this.map.updateArea( area2 );
        }
        break;
    }
  }

  selectedMarkersRemoved(indexes: number[]) {
    if ( this.map.getDrawMode() == 'point' ) {
      this.contextForm.get('geolocation').patchValue({});
      this.reachCity();
    }

    if ( this.map.getDrawMode() == 'multiPoints' ) {
      if ( indexes.length == 0 ) {
        return;
      }

      const points = this.contextForm.get('points').value;

      indexes.forEach(index => {
        points.splice(index, 1);
      });

      this.contextForm.get('points').patchValue( points );
      points.forEach((point, index) => {
        this.map.renderMarker( point, index );
      });

      if ( points.length == 1 ) {
        this.map.centerMap( points[0], 13 );
      }

      if ( points.length > 1 ) {
        this.map.centerMap( this.mapSvc.getCentroid( [points] ), 13 );
      }
    }
  }

  markerAllRemoved(evt: boolean) {
    this.contextForm.get('points').patchValue( [] );
  }

  areaDeleted(evt: boolean) {
    this.contextForm.get('area').patchValue([]);
  }

  toggleDescriptionField() {
    this.showDescriptionField = !this.showDescriptionField;
  }

  onValueChanged() {
    if (!this.contextForm) { return; }

    const form = this.contextForm;

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        this.formErrors[field] = '';

        const control = form.get(field);

        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  onNameChange(data: any) {
    if ( data ) {
      this.contextForm.get('slug').patchValue( this.slugifySvc.slugify( data ), { emitEvent: false } );
    }
  }

  onCityChange(data: any) {
    this.reachCity();
  }

  reachCity() {
    if ( this.cityData ) {
      this.cityData.forEach(city => {
        if ( city.key == this.contextForm.get('city').value ) {
          this.map.centerMap( [ city.geolocation.lng, city.geolocation.lat ], 10 );
        }
      });
    }
  }

  onGeoModeChange(data: any) {
    if ( data != this.map.getDrawMode() ) {
      this.map.setDrawMode( data );
    }
  }

  onCategoryChange(catID: any) {
    this.categories.subscribe(
      (data) => {
        data.forEach(cat => {
          if ( cat.key == catID ) {
            this.categoryTypes = cat.types;
          }
        });
      }
    );
  }

  addNewStakeholder() {
    this.contextForm.get('stakeholders').value.push( this.contextForm.get('newStakeholder').value );
  }

  deleteStakeholder(index: number) {
    this.contextForm.get('stakeholders').value.splice(index, 1);
    this.save();
  }

  addNewLink() {
    this.contextForm.get('links').value.push( this.contextForm.get('newLink').value );
  }

  deleteLink(index: number) {
    this.contextForm.get('links').value.splice(index, 1);
    this.save();
  }

  addRelatedAction(relActionObj: any) {
    let rels: any = [];
    let canAdd = true;
    if ( this.contextForm.get('relatedActions').value ) {
      rels = this.contextForm.get('relatedActions').value;
    }

    for (let i = 0; i < rels.length; i++) {
      if ( rels[i].key === relActionObj.key ) {
        canAdd = false;
      }
    }

    if (canAdd) {
      rels.push( relActionObj );
    } else {
      this.notify.update('Warning', 'Related action already added', 'warning');
    }

    this.contextForm.get('relatedActions').patchValue( rels );
  }

  deleteRelatedAction(index: number) {
    const rels = this.contextForm.get('relatedActions').value;
    rels.splice( index, 1 );
    this.contextForm.get('relatedActions').patchValue( rels );
  }

  addRelatedPractice(practiceID: string) {
    let rels;
    if ( this.contextForm.get('relatedPractices').value ) {
      rels = this.contextForm.get('relatedPractices').value;
    } else {
      rels = [];
    }

    rels.push( practiceID );
    this.contextForm.get('relatedPractices').patchValue( rels );
  }

  deleteRelatedPractice(index: number) {
    const rels = this.contextForm.get('relatedPractices').value;
    rels.splice( index, 1 );
    this.contextForm.get('relatedPractices').patchValue( rels );
  }

  addBlock(type: string) {
    switch (type) {
      case 'title':
        this.contextForm.get('blocks').value.push({
          type: 'title',
          content: 'Lorem ipsum dolor...'
        });
        break;
      case 'text':
        this.contextForm.get('blocks').value.push({
          type: 'text',
          content: 'Lorem ipsum dolor...'
        });
        break;
      case 'caption':
        this.contextForm.get('blocks').value.push({
          type: 'caption',
          content: 'Lorem ipsum dolor...'
        });
        break;
      case 'youtube':
        this.contextForm.get('blocks').value.push({
          type: 'youtube',
          content: 'Lorem ipsum dolor...'
        });
    }
  }

  deleteBlock(blockIndex: number) {
    this.contextForm.get('blocks').value.splice(blockIndex, 1);
  }

  onBlockChange(evt: any, index: number) {
    this.contextForm.get('blocks').value[index].content = evt;
  }

  onDrop(evt: any) {
    this.resetBlocksOrder();
    this.save();
  }

  resetBlocksOrder() {
    this.contextForm.get('blocks').value.forEach((context, i) => {
      context.order = i;
    });
  }

  onUploadResult(evt: any) {
    this.uploader.clearUploadQueue();

		if ( evt.status == 200 ) {
      this.notify.update('Keep going!', 'Done uploading', 'info');

			for (let i = 0; i < evt.files.length; ++i) {
        if ( evt.files[i].data.resource_type == 'image' ) {
          const imgs = this.contextForm.get('images').value;
          imgs.push( evt.files[i].data );
          this.contextForm.get('images').patchValue( imgs );
        }

        if ( evt.files[i].data.resource_type == 'video' ) {
          const videos = this.contextForm.get('videos').value;
          videos.push( evt.files[i].data );
          this.contextForm.get('videos').patchValue( videos );
        }
			}

			this.save();
		}
  }

  onUploadResultBlock(evt: any) {
    this.uploaderButton.clearUploadQueue();

		if ( evt.status == 200 ) {
      this.isUploadingBlock = false;
      this.notify.update('Great!', 'Done uploading', 'success');

			for (let i = 0; i < evt.files.length; ++i) {
        if ( evt.files[i].data.resource_type == 'image' ) {
          this.contextForm.get( 'blocks' ).value.push({
            type: 'image',
            content: evt.files[i].data
          });
        }

        if ( evt.files[i].data.resource_type == 'video' ) {
          this.contextForm.get( 'blocks' ).value.push({
            type: 'video',
            content: evt.files[i].data
          });
        }
			}
		}
  }

  onAfterAddingFile(evt: any) {
    // console.log( evt );
  }

  onAfterAddingFileBlock(evt: any) {
    if ( this.uploaderButton.responses.length > 0 ) {
      this.isUploadingBlock = true;
      this.uploaderButton.startUpload();
    }
  }

  deleteImage(index: number) {
    const imgs = this.contextForm.get('images').value;
    imgs.splice( index, 1 );
    this.contextForm.get('images').patchValue( imgs );
  }

  deleteVideo(index: number) {
    const videos = this.contextForm.get('videos').value;
    videos.splice( index, 1 );
    this.contextForm.get('videos').patchValue( videos );
  }

  createNewTag() {
    const newTagVal = this.contextForm.get('newTag').value;
    const newTagKey = this.slugifySvc.slugify( newTagVal );
    this.firebase.setObjectWithKey('tags/' + newTagKey, { label: newTagVal }).then(
      (data: any) => {
        console.log( data );
      },
      (err) => this.notify.update('Something went wrong!', err, 'error')
    );
  }

  addTag(evt: any) {
    let tags: any = [];
    let canAdd = true;
    if ( this.contextForm.get('tags').value ) {
      tags = this.contextForm.get('tags').value;
    }

    for (let i = 0; i < tags.length; i++) {
      if ( tags[i].key === evt.key ) {
        canAdd = false;
      }
    }

    if (canAdd) {
      tags.push( evt );
    } else {
      this.notify.update('Warning', 'Tag already added', 'warning');
    }

    this.contextForm.get('tags').patchValue( tags );
  }

  removeTag(index: number) {
    const tags = this.contextForm.get('tags').value;
    tags.splice( index, 1 );
    this.contextForm.get('tags').patchValue( tags );
  }

  save() {
    if ( this.uploader.responses.length > 0 ) {
      this.uploader.startUpload();
      return;
    }

    this.isSaving = true;

    this.contextForm.patchValue({ lastUpdated: Date.now() });

    if ( this.isNewItem ) {
      this.firebase.createObject( `actions`, this.contextForm.getRawValue() ).then(
        (data: any) => {
          this.notify.update('Great!', this.itemSingular + ' created', 'success');
          this.isSaving = false;
          this.router.navigate(['/actions/edit/' + data.key]);
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    } else {
      // console.log( this.contextForm.getRawValue() );
      this.firebase.updateObject( `actions/${ this.objectKey }`, this.contextForm.getRawValue() ).then(
        () => {
          this.isSaving = false;
          this.notify.update('Great!', this.itemSingular + ' updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }
}
