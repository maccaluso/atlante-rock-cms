import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { faPlus, faCheckCircle, faBan } from '@fortawesome/free-solid-svg-icons';

import { NotifyService } from "../../shared/services/notify.service";
import { FirebaseService } from "../../shared/services/firebase.service";

@Component({
  selector: 'type-list',
  templateUrl: './type-list.component.html',
  styleUrls: ['./type-list.component.scss']
})
export class TypeListComponent implements OnInit {

  types: Observable<any>;
  showSpinner: boolean = true;
  isDeletingItem: boolean = false;
  itemToDelete: number;

  faPlus = faPlus;
  faCheckCircle = faCheckCircle;
  faBan = faBan;

  constructor(
    private firebase: FirebaseService,
    private notify: NotifyService
  ) {}

  ngOnInit() {
    this.types = this.firebase.getList('/rock-category-types');
    this.types.subscribe(
      (data) => {
        this.showSpinner = false;
      },
      (err) => console.log(err),
      () => console.log('complete')
    );
  }

  deleteItem(key: string, index: number) {
    this.isDeletingItem = true;
    this.itemToDelete = index;
    this.firebase.deleteObject('rock-category-types', key).then(
      () => {
        this.isDeletingItem = false;
        this.notify.update('Great!', 'Item deleted', 'success');
      },
      (err) => console.log( err )
    );
  }

}
