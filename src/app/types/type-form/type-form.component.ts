import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";
import { Observable } from 'rxjs';

import { faBan, faCheck, faPlus } from '@fortawesome/free-solid-svg-icons';

import { FirebaseService } from "../../shared/services/firebase.service";
import { SlugifyService } from "../../shared/services/slugify.service";
import { NotifyService } from "../../shared/services/notify.service";

@Component({
  selector: 'type-form',
  templateUrl: './type-form.component.html',
  styleUrls: ['./type-form.component.scss']
})
export class TypeFormComponent implements OnInit {

  debug: boolean = false;

  type: Observable<any>;
  objectKey: string;
  symbols: any;
  shapes: any = [ 'square', 'circle', 'rhombus', 'hexagon', 'triangle', 'cross' ];
  isNewItem: boolean = true;
  itemSingular: string = 'type';
  itemPlural: string = 'types';
  isSaving = false;

  typeForm: FormGroup;
  formErrors = {
    'name': '',
    'slug': '',
    'symbol': '',
    'city': '',
    'description': ''
  };
  validationMessages = {
    'name': {
      'required': 'Name is required.'
    },
    'slug': {
      'required': 'Slug is required.'
    },
    'symbol': {
      'required': 'Symbol is required.'
    }
  };

  faCheck = faCheck;
  faBan = faBan;
  faPlus = faPlus;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService
  ) {}

  ngOnInit() {
    this.typeForm = this.fb.group({
      name: [ '', Validators.required ],
      slug: [ '', Validators.required ],
      symbol: [ '', Validators.required ],
      description: '',
      created: '',
      lastUpdated: ''
    });      

    this.typeForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged();

    this.route.params.subscribe(
      (data) => {
        if(data.id) {
          this.isNewItem = false;

          this.type = this.firebase.getObject('/rock-category-types/' + data.id)
          this.type.subscribe(
            (data) => {
              this.objectKey = data.key;

              this.typeForm.setValue({
                name: data.name ? data.name : null,
                slug: data.slug ? data.slug : null,
                symbol: data.symbol ? data.symbol : null,
                description: data.description ? data.description : null,
                created: data.created ? data.created : Date.now(),
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now()
              });
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );
  }

  selectSymbol(sym: any) {
    console.log(sym);
    this.typeForm.get('symbol').patchValue( sym );
  }

  onValueChanged(data?: any) {    
    if (!this.typeForm) { return; }
    
    const form = this.typeForm;

    if(data)
      form.get('slug').patchValue( this.slugifySvc.slugify( data.name ), { emitEvent: false } );

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        // clear previous error message (if any)
        this.formErrors[field] = '';

        const control = form.get(field);
        
        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  save() {
    this.isSaving = true;

    this.typeForm.patchValue({ lastUpdated: Date.now() });

    if( this.isNewItem )
    {
      this.firebase.createObject( `rock-category-types`,  this.typeForm.getRawValue() ).then(
        () => {
          this.notify.update('Great!', 'Item created', 'success');
          this.isSaving = false;
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
    else
    {
      this.firebase.updateObject( `rock-category-types/${ this.objectKey }`,  this.typeForm.getRawValue() ).then(
        () => {
          this.isSaving = false;
          this.notify.update('Great!', 'Item updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }

}
