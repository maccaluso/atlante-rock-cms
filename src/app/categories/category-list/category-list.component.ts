import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { faPlus, faCheckCircle, faBan } from '@fortawesome/free-solid-svg-icons';

import { NotifyService } from "../../shared/services/notify.service";
import { FirebaseService } from "../../shared/services/firebase.service";

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  categories: Observable<any>;
  showSpinner: boolean = true;
  isDeletingItem: boolean = false;
  itemToDelete: number;

  faPlus = faPlus;
  faCheckCircle = faCheckCircle;
  faBan = faBan;

  constructor(
    private firebase: FirebaseService,
    private notify: NotifyService
  ) {}

  ngOnInit() {
    this.categories = this.firebase.getList('/rock-categories');
    this.categories.subscribe(
      (data) => {
        this.showSpinner = false;
      },
      (err) => console.log(err),
      () => console.log('complete')
    );
  }

  deleteItem(key: string, index: number) {
    this.isDeletingItem = true;
    this.itemToDelete = index;
    this.firebase.deleteObject('rock-categories', key).then(
      () => {
        this.isDeletingItem = false;
        this.notify.update('Great!', 'Item deleted', 'success');
      },
      (err) => console.log( err )
    );
  }

}
