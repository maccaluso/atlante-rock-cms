import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";
import { Observable } from 'rxjs';

import { faBan, faCheck, faPlus, faUpload } from '@fortawesome/free-solid-svg-icons';

import { FormUploaderComponent } from '../../shared/widgets/form-uploader/form-uploader.component';
import { FormUploaderButtonComponent } from "../../shared/widgets/form-uploader-button/form-uploader-button.component";
import { FirebaseService } from "../../shared/services/firebase.service";
import { SlugifyService } from "../../shared/services/slugify.service";
import { NotifyService } from "../../shared/services/notify.service";

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {
  @ViewChild( FormUploaderComponent ) uploader: FormUploaderComponent;
  @ViewChild( FormUploaderButtonComponent ) uploaderButton: FormUploaderButtonComponent;

  debug: boolean = false;

  category: Observable<any>;
  types: Observable<any[]>;
  objectKey: string;
  isNewItem: boolean = true;
  itemSingular: string = 'category';
  itemPlural: string = 'categories';
  isSaving = false;

  categoryForm: FormGroup;
  formErrors = {
    'color': '',
    'name': '',
    'slug': '',
    'description': ''
  };
  validationMessages = {
    'color': {
      'required': 'Color is required.'
    },
    'name': {
      'required': 'Name is required.'
    },
    'slug': {
      'required': 'Slug is required.'
    }
  };

  faCheck = faCheck;
  faBan = faBan;
  faPlus = faPlus;
  faUpload = faUpload;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    this.categoryForm = this.fb.group({
      name: [ '', Validators.required ],
      slug: [ '', Validators.required ],
      color: [ '', Validators.required ],
      strokeWidth: 2,
      isGeneral: 'false',
      icon: '',
      icon2: '',
      description: '',
      types: [],
      created: '',
      lastUpdated: ''
    });      

    this.categoryForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged();

    this.route.params.subscribe(
      (data) => {
        if(data.id) {
          this.isNewItem = false;

          this.category = this.firebase.getObject('/rock-categories/' + data.id)
          this.category.subscribe(
            (data) => {
              this.objectKey = data.key;

              this.categoryForm.setValue({
                name: data.name ? data.name : null,
                slug: data.slug ? data.slug : null,
                color: data.color ? data.color : null,
                strokeWidth: data.strokeWidth ? data.strokeWidth : 2,
                isGeneral: data.isGeneral ? data.isGeneral : 'false',
                icon: data.icon ? data.icon : '',
                icon2: data.icon2 ? data.icon2 : '',
                description: data.description ? data.description : null,
                types: data.types ? data.types : [],
                created: data.created ? data.created : Date.now(),
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now()
              });
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );

    this.types = this.firebase.getList('rock-category-types');
  }

  onValueChanged(data?: any) {    
    if (!this.categoryForm) { return; }
    
    const form = this.categoryForm;

    if(data)
      form.get('slug').patchValue( this.slugifySvc.slugify( data.name ), { emitEvent: false } );

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        // clear previous error message (if any)
        this.formErrors[field] = '';

        const control = form.get(field);
        
        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  onUploadResult(evt: any) {
    // console.log( evt );
    this.uploader.clearUploadQueue();

		if( evt.status == 200 ) {
      this.notify.update('Keep going!', 'Done uploading', 'info');

      if( evt.files.length > 0 && evt.files.length <= 2 )
      {
        for (var i = 0; i < evt.files.length; ++i) {
          if( evt.files[i].data.resource_type == 'image' )
          {
            if( i == 0 )
            {
              let icon = this.categoryForm.get('icon').value;
              icon = evt.files[i].data;
              this.categoryForm.get('icon').patchValue( icon );
            }
            if( i == 1 )
            {
              let icon2 = this.categoryForm.get('icon2').value;
              icon2 = evt.files[i].data;
              this.categoryForm.get('icon2').patchValue( icon2 );
            }
          }
        }
  
        this.save();
      }
      else
      {
        console.log( 'no way' );
      }
		}
  }

  onAfterAddingFile(evt: any) {
    // console.log( evt );
  }

  deleteIcon() {
    this.categoryForm.get('icon').patchValue( '' );
  }

  save() {
    if( this.uploader.responses.length > 0 )
		{
      this.uploader.startUpload();
      return;
    }

    this.isSaving = true;

    this.categoryForm.patchValue({ lastUpdated: Date.now() });

    if( this.isNewItem )
    {
      this.firebase.createObject( `rock-categories`,  this.categoryForm.getRawValue() ).then(
        () => {
          this.notify.update('Great!', 'Item created', 'success');
          this.isSaving = false;
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
    else
    {
      this.firebase.updateObject( `rock-categories/${ this.objectKey }`,  this.categoryForm.getRawValue() ).then(
        () => {
          this.isSaving = false;
          this.notify.update('Great!', 'Item updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }

}
