import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { faBan, faCheck, faUpload } from '@fortawesome/free-solid-svg-icons';

import { FormUploaderComponent } from '../../shared/widgets/form-uploader/form-uploader.component';
import { FormUploaderButtonComponent } from '../../shared/widgets/form-uploader-button/form-uploader-button.component';
import { FirebaseService } from '../../shared/services/firebase.service';
import { SlugifyService } from '../../shared/services/slugify.service';
import { NotifyService } from '../../shared/services/notify.service';
import { MapBoxComponent } from '../../shared/widgets/map-box/map-box.component';

@Component({
  selector: 'app-practice-form',
  templateUrl: './practice-form.component.html',
  styleUrls: ['./practice-form.component.scss']
})
export class PracticeFormComponent implements OnInit, AfterViewInit {
  @ViewChild( MapBoxComponent ) map: MapBoxComponent;
  @ViewChild( FormUploaderComponent ) uploader: FormUploaderComponent;
  @ViewChild( FormUploaderButtonComponent ) uploaderButton: FormUploaderButtonComponent;

  debug = false;

  context: Observable<any>;
  cities: Observable<any>;
  categories: Observable<any>;
  actions: Observable<any>;
  practices: Observable<any>;
  categoryTypes: any;
  location: number[];
  objectKey: string;
  isNewItem = true;
  itemSingular = 'Model practice';
  itemPlural = 'Model practices';
  isSaving = false;
  isUploadingBlock = false;
  geoMode = 'point';
  storyModalOpen = false;

  contextForm: FormGroup;
  formErrors = {
    'name': '',
    'slug': '',
    'type': '',
    'geolocation': '',
    'description': '',
    'images': '',
    'videos': '',
    'active': '',
    'created': '',
    'lastUpdated': '',
    'author': '',
    'city': '',
    'ROCKcategory': ''
  };
  validationMessages = {
    'name': {
      'required': 'Name is required.'
    },
    'slug': {
      'required': 'Slug is required.'
    },
    'type': {
      'required': 'Type is required.'
    },
    'ROCKcategory': {
      'required': 'ROCK Category is required.'
    }
  };

  dpConfig: any = {
    drops: 'down',
    attachTo: '.control'
  };

  faCheck = faCheck;
  faBan = faBan;
  faUpload = faUpload;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    this.contextForm = this.fb.group({
      address: this.fb.group({
        country: '',
        locality: '',
        street: '',
        zipcode: ''
      }),
      active: true,
      area: [],
      blocks: [],
      city: {},
      created: '',
      description: '',
      endDate: '',
      geolocation: [ { lng: '', lat: '' }, Validators.required ],
      images: [],
      lastUpdated: '',
      links: [],
      newLink: this.fb.group({
        label: '',
        url: ''
      }),
      name: [ '', Validators.required ],
      order: 0,
      points: [],
      relatedActions: [],
      relatedPractices: [],
      ROCKcategory: {},
      ROCKtype: {},
      slug: [ '', Validators.required ],
      stakeholders: [],
      newStakeholder: this.fb.group({
        name: '',
        url: ''
      }),
      startDate: '',
      status: '',
      substatus: '',
      summary: '',
      type: [ '', Validators.required ],
      videos: []
    });

    this.contextForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.contextForm.get('name').valueChanges.subscribe((data) => this.onNameChange(data));
    this.contextForm.get('city').valueChanges.subscribe((data) => this.onCityChange(data));
    this.contextForm.get('type').valueChanges.subscribe((data) => this.onGeoModeChange(data));
    this.contextForm.get('ROCKcategory').valueChanges.subscribe((data) => this.onCategoryChange(data));

    this.route.params.subscribe(
      (data) => {
        if (data.id) {
          this.isNewItem = false;

          this.context = this.firebase.getObject('/best-practices/' + data.id);
          this.context.subscribe(
            (data) => {
              this.objectKey = data.key;
              if ( data.geolocation ) {
                this.location = [ data.geolocation.lng, data.geolocation.lat ];
              }

              this.contextForm.setValue({
                address: data.address ? data.address : {
                  country: '',
                  locality: '',
                  street: '',
                  zipcode: ''
                },
                active: data.active ? data.active : true,
                area: data.area ? data.area : [],
                blocks: data.blocks ? data.blocks : [],
                points: data.points ? data.points : [],
                city: data.city ? data.city : '',
                created: data.created ? data.created : Date.now(),
                description: data.description ? data.description : null,
                endDate: data.endDate ? data.endDate : '',
                geolocation: data.geolocation ? data.geolocation : { lng: '', lat: '' },
                images: data.images ? data.images : [],
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now(),
                links: data.links ? data.links : [],
                newLink: {
                  label: '',
                  url: ''
                },
                name: data.name ? data.name : null,
                order: data.order ? data.order : 0,
                relatedActions: data.relatedActions ? data.relatedActions : [],
                relatedPractices: data.relatedPractices ? data.relatedPractices : [],
                ROCKcategory: data.ROCKcategory ? data.ROCKcategory : null,
                ROCKtype: data.ROCKtype ? data.ROCKtype : null,
                slug: data.slug ? data.slug : null,
                stakeholders: data.stakeholders ? data.stakeholders : [],
                newStakeholder: {
                  name: '',
                  url: ''
                },
                startDate: data.startDate ? data.startDate : '',
                status: data.status ? data.status : null,
                substatus: data.substatus ? data.substatus : null,
                summary: data.summary ? data.summary : null,
                type: data.type ? data.type : this.geoMode,
                videos: data.videos ? data.videos : []
              });
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );

    this.cities = this.firebase.getFilteredList('cities', 'type', 'model');
    this.categories = this.firebase.getList('rock-categories');
    this.categories.subscribe(
      (data) => {
        data.forEach((cat: any) => {
          if ( cat.key === this.contextForm.get('ROCKcategory').value ) {
            this.categoryTypes = cat.types;
            console.log(cat.types);
          }
        });
      }
    );
    this.actions = this.firebase.getList('/actions');
    this.practices = this.firebase.getList('/best-practices');
  }

  ngAfterViewInit() {
    if (this.isNewItem) {
      const tm = setTimeout(() => {
        this.contextForm.get('geolocation').patchValue({ lng: this.map.defaultLocation[0], lat: this.map.defaultLocation[1] });
        clearTimeout( tm );
      });
    }
  }

  onValueChanged(data?: any) {
    if (!this.contextForm) { return; }

    const form = this.contextForm;

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        this.formErrors[field] = '';

        const control = form.get(field);

        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  onNameChange(data: any) {
    this.contextForm.get('slug').patchValue( this.slugifySvc.slugify( data ), { emitEvent: false } );
  }

  onCityChange(data?: any) {
    // console.log(data);
    this.firebase.getObject( 'cities/' +  data ).subscribe(
      (city: any) => {
        this.map.flyTo( [city.geolocation.lng, city.geolocation.lat], this.map.getCurrentZoom() );
      }
    );
  }

  onGeoModeChange(data?: any) {
    this.geoMode = this.map.drawMode = data;

    if ( this.geoMode != 'point' && this.map.mapIsLoaded ) {
      this.map.switchAreaType();
    }

    if ( this.geoMode == 'point' ) {
      this.map.removeArea();
    }
  }

  onCategoryChange(catID: any) {
    this.categories.subscribe(
      (data) => {
        data.forEach(cat => {
          if ( cat.key == catID ) {
            this.categoryTypes = cat.types;
          }
        });
      }
    );
  }

  addNewStakeholder() {
    this.contextForm.get('stakeholders').value.push( this.contextForm.get('newStakeholder').value );
  }

  deleteStakeholder(index: number) {
    this.contextForm.get('stakeholders').value.splice(index, 1);
    this.save();
  }

  addNewLink() {
    this.contextForm.get('links').value.push( this.contextForm.get('newLink').value );
  }

  deleteLink(index: number) {
    this.contextForm.get('links').value.splice(index, 1);
    this.save();
  }

  addRelatedAction(actionID: string) {
    this.contextForm.get('relatedActions').value.push( actionID );
  }

  deleteRelatedAction(index: number) {
    this.contextForm.get('relatedActions').value.splice( index, 1 );
  }

  addRelatedPractice(practiceID: string) {
    this.contextForm.get('relatedPractices').value.push( practiceID );
  }

  deleteRelatedPractice(index: number) {
    this.contextForm.get('relatedPractices').value.splice( index, 1 );
  }

  addBlock(type: string) {
    switch (type) {
      case 'title':
        this.contextForm.get('blocks').value.push({
          type: 'title',
          content: 'Lorem ipsum dolor...'
        });
        break;
      case 'text':
        this.contextForm.get('blocks').value.push({
          type: 'text',
          content: 'Lorem ipsum dolor...'
        });
        break;
    }
  }

  onBlockChange(evt: any, index: number) {
    // console.log(evt, index);
    this.contextForm.get('blocks').value[index].content = evt;
  }

  onDrop(evt: any) {
    this.resetBlocksOrder();
    this.save();
  }

  resetBlocksOrder() {
    this.contextForm.get('blocks').value.forEach((context, i) => {
      context.order = i;
    });
  }

  onMapLoad(evt: any) {}

  onMapClick(evt: any) {
    switch (this.geoMode) {
      case 'point':
        this.setPoint( evt );
        break;
      case 'multiPoints':
        this.setMultiplePoints( evt );
        break;
      case 'areaStroke':
        this.setArea( evt );
        break;
      case 'areaFill':
        this.setArea( evt );
        break;
    }
  }

  setPoint(evt: any) {
    this.contextForm.patchValue({
      geolocation: { lng: evt.data[0], lat: evt.data[1] }
    });
    this.location = [ evt.data[0], evt.data[1] ];
  }

  setMultiplePoints(evt: any) {
    this.contextForm.patchValue({
      points: evt.data
    });
  }

  removeMultiPoint(index: number) {
    const points = this.contextForm.get('points').value;
    points.splice( index, 1 );
    this.contextForm.patchValue({
      points: points
    });

    this.map.markers.splice( index, 1 );
  }

  setArea(evt: any) {
    this.contextForm.patchValue({
      area: evt.data
    });
  }

  deleteArea() {
    this.map.removeArea();
    this.contextForm.get('area').patchValue([]);
  }

  onUploadResult(evt: any) {
    this.uploader.clearUploadQueue();

		if ( evt.status == 200 ) {
      this.notify.update('Keep going!', 'Done uploading', 'info');

			for (let i = 0; i < evt.files.length; ++i) {
        if ( evt.files[i].data.resource_type == 'image' ) {
          const imgs = this.contextForm.get('images').value;
          imgs.push( evt.files[i].data );
          this.contextForm.get('images').patchValue( imgs );
        }

        if ( evt.files[i].data.resource_type == 'video' ) {
          const videos = this.contextForm.get('videos').value;
          videos.push( evt.files[i].data );
          this.contextForm.get('videos').patchValue( videos );
        }
			}

			this.save();
		}
  }

  onUploadResultBlock(evt: any) {
    this.uploaderButton.clearUploadQueue();

		if ( evt.status == 200 ) {
      this.isUploadingBlock = false;
      this.notify.update('Great!', 'Done uploading', 'success');

			for (let i = 0; i < evt.files.length; ++i) {
        if ( evt.files[i].data.resource_type == 'image' ) {
          this.contextForm.get( 'blocks' ).value.push({
            type: 'image',
            content: evt.files[i].data
          });
        }

        if ( evt.files[i].data.resource_type == 'video' ) {
          this.contextForm.get( 'blocks' ).value.push({
            type: 'video',
            content: evt.files[i].data
          });
        }
			}
		}
  }

  onAfterAddingFile(evt: any) {
    // console.log( evt );
  }

  onAfterAddingFileBlock(evt: any) {
    if ( this.uploaderButton.responses.length > 0 ) {
      this.isUploadingBlock = true;
      this.uploaderButton.startUpload();
    }
  }

  deleteImage(index: number) {
    const imgs = this.contextForm.get('images').value;
    imgs.splice( index, 1 );
    this.contextForm.get('images').patchValue( imgs );
  }

  deleteVideo(index: number) {
    const videos = this.contextForm.get('videos').value;
    videos.splice( index, 1 );
    this.contextForm.get('videos').patchValue( videos );
  }

  save() {
    if ( this.uploader.responses.length > 0 ) {
      this.uploader.startUpload();
      return;
    }

    this.isSaving = true;

    this.contextForm.patchValue({ lastUpdated: Date.now() });

    if ( this.isNewItem ) {
      this.firebase.createObject( `best-practices`,  this.contextForm.getRawValue() ).then(
        () => {
          this.notify.update('Great!', this.itemSingular + ' created', 'success');
          this.isSaving = false;
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    } else {
      this.firebase.updateObject( `best-practices/${ this.objectKey }`,  this.contextForm.getRawValue() ).then(
        () => {
          this.isSaving = false;
          this.notify.update('Great!', this.itemSingular + ' updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }
}
