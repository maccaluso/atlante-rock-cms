import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { FileUploadModule } from 'ng2-file-upload';
import { DndModule } from 'ng2-dnd';
import { DpDatePickerModule } from 'ng2-date-picker'

import { WidgetsModule } from '../shared/widgets/widgets.module';

import { PracticeListComponent } from './practice-list/practice-list.component';
import { PracticeFormComponent } from './practice-form/practice-form.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    CloudinaryModule,
    FileUploadModule,
    DndModule.forRoot(),
    DpDatePickerModule,
    WidgetsModule
  ],
  declarations: [
    PracticeListComponent, 
    PracticeFormComponent
  ]
})
export class PracticeModule { }