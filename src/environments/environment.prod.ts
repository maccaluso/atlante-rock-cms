export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyDq112LMAgKXcqU1xqz2eQkPH4x0aQ9Bwg",
    authDomain: "atlante-rock.firebaseapp.com",
    databaseURL: "https://atlante-rock.firebaseio.com",
    projectId: "atlante-rock",
    storageBucket: "atlante-rock.appspot.com",
    messagingSenderId: "128304408510"
  },
  mapbox: {
    accessToken: 'pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6ImNqZHY1OXVodDAwNm8zM3J1bDJhYzZlZjEifQ.h4x22nzctRwvH7eoOr7mhA'
  },
  cloudinary: {
    cloud_name: 'gianluca-macaluso', 
    upload_preset: 'ROCK-Atlas'
  }
};